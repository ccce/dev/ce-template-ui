import { EssThemeProvider } from "@ess-ics/ce-ui-common";
import { ReactNode } from "react";
import { theme } from "../style/Theme";
interface MuiThemeProviderProps {
  children: ReactNode;
}

export const MuiThemeProvider = ({ children }: MuiThemeProviderProps) => {
  return <EssThemeProvider theme={theme}>{children}</EssThemeProvider>;
};
