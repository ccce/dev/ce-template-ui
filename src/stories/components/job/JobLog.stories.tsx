import { JobLogSection } from "../../../views/job/JobDetailsView/JobLogSection/JobLogSection";
import jobsList from "../../../mocks/fixtures/jobDetails.json";
import statusList from "../../../mocks/fixtures/jobStatus.json";
import { JobStatusApiResponse, JobDetails } from "../../../store/templateApi";
import type { Meta, StoryObj } from "@storybook/react";

const Template = ({ ...args }) => {
  const statusItem =
    statusList.find((statusItem) => statusItem.status === args.jobStatus) ||
    statusList[0];
  const job =
    jobsList.find((op) => op.job_id === statusItem?.job_id) || jobsList[0];

  return (
    <JobLogSection
      jobDetails={job as JobDetails}
      jobStatus={statusItem as JobStatusApiResponse}
    />
  );
};

const meta: Meta = {
  title: "Job log section",
  component: Template
};
export default meta;

type Story = StoryObj<typeof Template>;

export const Default: Story = {
  args: {
    jobStatus: "SUCCESSFUL"
  },
  argTypes: {
    jobStatus: {
      options: ["QUEUED", "RUNNING", "SUCCESSFUL", "FAILED", "PARTIAL_FAILURE"],
      control: { type: "radio" }
    }
  }
};
