import { STEPPER_STATES } from "@ess-ics/ce-ui-common";
import { InstanceJobLog } from "../../../views/job/JobDetailsView/JobLogSection/InstanceJobLog";
import { InstanceLogEntry } from "../../../store/templateApi";
import type { Meta, StoryObj } from "@storybook/react";

const meta: Meta = {
  title: "Intance Job log",
  component: InstanceJobLog
};
export default meta;

type Story = StoryObj<typeof InstanceJobLog>;

type StepType =
  | "CREATE_GIT_PROJECT"
  | "PROCESSING_TEMPLATE_AND_COMMIT"
  | undefined;

const steps = ["CREATE_GIT_PROJECT", "PROCESSING_TEMPLATE_AND_COMMIT"];

const log = (index: number, isFailed: boolean): InstanceLogEntry => {
  let step: StepType;
  if (index < steps.length) {
    step = steps[index] as StepType;
  }
  return {
    step: step,
    step_index: index,
    status: isFailed ? STEPPER_STATES.FAILED : STEPPER_STATES.SUCCESSFUL,
    created_at: new Date().toISOString(),
    message: isFailed ? "failure!" : ""
  };
};

export const Default: Story = {
  args: {
    logs: [log(0, false), log(1, false)]
  }
};

export const WithFailure: Story = {
  args: {
    logs: [log(0, false), log(1, true)]
  }
};
