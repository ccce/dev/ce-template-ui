import { JobStatusSection } from "../../../views/job/JobDetailsView/JobStatusSection";
import type { Meta, StoryObj } from "@storybook/react";

const meta: Meta = {
  title: "JobDetails",
  component: JobStatusSection
};
export default meta;

type Story = StoryObj<typeof JobStatusSection>;

export const Status: Story = {
  argTypes: {
    status: {
      options: ["QUEUED", "RUNNING", "SUCCESSFUL", "FAILED", "PARTIAL_FAILURE"],
      control: { type: "radio" }
    }
  },
  args: {
    status: "SUCCESSFUL"
  }
};
