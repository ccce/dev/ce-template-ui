import AddIcon from "@mui/icons-material/Add";
import {
  ActionCard,
  ActionCardContent
} from "../../../../components/common/ActionCard/ActionCard";

export default {
  title: "ActionCard",
  argTypes: {
    onClick: { action: "Card is clicked", control: null }
  }
};

interface ActionCardContentProps {
  title: string;
  titleProps: Record<string, unknown>;
  subtitle: string;
  subtitleProps: Record<string, unknown>;
  SvgIcon: React.ElementType;
}

interface CardWithActionContentProps extends ActionCardContentProps {
  onClick: () => void;
}

export const CardBlank = (args: { onClick: () => void }) => (
  <ActionCard {...args}>
    <></>
  </ActionCard>
);

export const CardWithActionContent = (args: CardWithActionContentProps) => (
  <ActionCard {...args}>
    <ActionCardContent {...args} />
  </ActionCard>
);

CardWithActionContent.args = {
  title: "Create from Blank",
  titleProps: { component: "h2" },
  subtitle: "Create a blank IOC type that can be used to generate IOCs",
  subtitleProps: { component: "h3" },
  SvgIcon: AddIcon,
  onClick: () => console.debug("Card is clicked")
};
