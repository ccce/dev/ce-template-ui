import { FileList } from "../../../components/generate/GenerateTemplate/ConfigFileSelector/FileList";
import configFiles from "../../../mocks/fixtures/configFiles.json";
import type { Meta, StoryObj } from "@storybook/react";

const Template = () => {
  return <FileList configFiles={configFiles} />;
};

const meta: Meta = {
  title: "GenerateTemplate/FileList",
  component: Template
};
export default meta;

type Story = StoryObj<typeof Template>;

export const Default: Story = {};
