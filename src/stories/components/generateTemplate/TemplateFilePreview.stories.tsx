import { TemplateFilePreview } from "../../../components/generate/GenerateTemplate/FilePreviewPane/TemplateFilePreview";
import type { Meta, StoryObj } from "@storybook/react";

// Needs MSW for mocking API calls

interface TemplateProps {
  selectedTemplateFile: string;
}

const Template = ({ selectedTemplateFile }: TemplateProps) => (
  <TemplateFilePreview selectedTemplateFile={selectedTemplateFile} />
);

const meta: Meta = {
  title: "GenerateTemplate/TemplateFilePreview",
  component: Template
};

export default meta;

type Story = StoryObj<typeof Template>;

const defaultArgs = {
  selectedTemplateFile: "some-file"
};

export const Default: Story = {
  args: {
    ...defaultArgs,
    selectedTemplateFile: "st.cmd.mustache"
  }
};
export const NoSubstitutedContent: Story = {
  args: {
    ...defaultArgs,
    selectedTemplateFile: ""
  }
};
