import { Typography } from "@mui/material";
import { FilePreview } from "../../../components/generate/GenerateTemplate/FilePreviewPane/FilePreview";
import { ApiError } from "../../../types/common";
import type { Meta, StoryObj } from "@storybook/react";

interface TemplateProps {
  isLoading: boolean;
  isError: boolean;
  value: string;
  useNoValueOverlay: boolean;
  language?: string;
}

const Template = ({
  isLoading,
  isError,
  value,
  useNoValueOverlay,
  language
}: TemplateProps) => {
  const error: ApiError = {
    status: 500,
    data: {}
  };

  return (
    <FilePreview
      isLoading={isLoading}
      value={value}
      error={isError ? error : undefined}
      renderOnNoValue={
        useNoValueOverlay
          ? () => <Typography color="white">No Value Provided</Typography>
          : undefined
      }
      language={language}
    />
  );
};

const meta: Meta = {
  title: "GenerateTemplate/FilePreview",
  component: Template
};
export default meta;

type Story = StoryObj<typeof Template>;

export const Default: Story = {
  args: {
    isLoading: false,
    isError: false,
    value: "{{some content}}",
    useNoValueOverlay: false,
    language: "handlebars"
  }
};
