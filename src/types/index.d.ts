export {};

declare global {
  interface Window {
    SERVER_ADDRESS: string;
    ENVIRONMENT_TITLE: string;
    API_BASE_ENDPOINT: string;
    SWAGGER_UI_URL: string;
    TOKEN_RENEW_INTERVAL: number;
    NAMING_ADDRESS: string;
    TEMPLATE_FRONTEND_VERSION: string;
    SUPPORT_URL: string;
    DOCS_URL: string;
  }
}
