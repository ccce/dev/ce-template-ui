import { FetchBaseQueryError } from "@reduxjs/toolkit/query";
import { SerializedError } from "@reduxjs/toolkit";

// TODO: Add these types from common when that is implemented
export interface GlobalAppBarContext {
  title: string;
  setTitle: (title: string) => void;
  setButton: () => void;
}

export interface User {
  fullName: string;
  loginName: string;
  avatar: string;
  email: string | null;
  gitlabUserName: string;
  roles: string[];
}

export interface OnPageParams {
  page: number;
  rows: number;
  rowsPerPageOptions?: number[];
}

export type ApiError = FetchBaseQueryError | SerializedError | undefined;

export interface ErrorMessage {
  data: {
    error: string;
    description: string;
  };
  status: number;
}

export interface ApiResult<T> {
  isLoading: boolean;
  error?: ApiError;
  data?: T;
}
