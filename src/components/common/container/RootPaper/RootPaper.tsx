/**
 * RootPaper
 * Styled wrapper component
 */
import { styled } from "@mui/material/styles";
import Paper from "@mui/material/Paper";

export const RootPaper = styled(Paper)(({ theme }) => ({
  paddingTop: theme.spacing(2),
  paddingBottom: theme.spacing(2),
  paddingLeft: theme.spacing(4),
  paddingRight: theme.spacing(4),
  marginBottom: "100px",
  maxWidth: "1400px",
  width: "100%"
}));
