import { Avatar, Tooltip } from "@mui/material";
import { number, string } from "prop-types";

const propTypes = {
  createdBy: string,
  avatar: string,
  size: number
};

interface UserAvatarProps {
  createdBy: string;
  avatar?: string;
  size?: number;
}

export const UserAvatar = ({
  createdBy,
  avatar,
  size = 48
}: UserAvatarProps) => {
  const altText = `Created by ${createdBy}`;
  return (
    <Tooltip title={createdBy}>
      {!avatar ? (
        <Avatar
          sx={{ width: size, height: size }}
          alt={altText}
          variant="circular"
        >
          {createdBy?.toUpperCase().slice(0, 2)}
        </Avatar>
      ) : (
        <Avatar
          sx={{ width: size, height: size }}
          src={avatar}
          alt={altText}
          variant="circular"
        />
      )}
    </Tooltip>
  );
};

UserAvatar.propTypes = propTypes;
