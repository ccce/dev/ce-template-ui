import {
  Box,
  Card,
  CardActionArea,
  CardContent,
  Typography,
  styled
} from "@mui/material";

interface ActionCardContentProps {
  title: string;
  titleProps?: object;
  subtitle: string;
  subtitleProps?: object;
  SvgIcon: React.ElementType;
  className?: string;
}

export const ActionCardContent = styled(
  ({
    title,
    titleProps,
    subtitle,
    subtitleProps,
    SvgIcon,
    className
  }: ActionCardContentProps) => {
    return (
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          gap: 1
        }}
        className={className}
      >
        <SvgIcon
          viewBox="0 0 25 25"
          sx={{
            height: "100%",
            width: "100px",
            filter: "opacity(0.6)"
          }}
        />
        <CardContent
          sx={{
            display: "flex",
            height: "100%",
            padding: "0 !important",
            flexDirection: "column",
            justifyContent: "space-between",
            gap: 2
          }}
        >
          <Typography
            variant="h3"
            {...titleProps}
          >
            {title}
          </Typography>
          <Typography
            variant="subtitle1"
            component="p"
            {...subtitleProps}
          >
            {subtitle}
          </Typography>
        </CardContent>
      </Box>
    );
  }
)({});

interface ActionCardProps {
  onClick: () => void;
  children: React.ReactNode;
  className?: string;
}

export const ActionCard = styled(
  ({ onClick, children, className }: ActionCardProps) => {
    return (
      <Card
        sx={{
          minWidth: 200,
          maxWidth: 400,
          height: 200,
          padding: 2,
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}
        className={className}
      >
        <CardActionArea
          sx={{ height: "100% !important" }}
          onClick={onClick}
        >
          {children}
        </CardActionArea>
      </Card>
    );
  }
)({});
