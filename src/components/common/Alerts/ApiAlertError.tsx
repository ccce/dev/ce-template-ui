import { Alert } from "@mui/material";
import { getErrorMessage } from "./ErrorHandling";
import { ApiError } from "../../../types/common";

interface ApiAlertErrorProps {
  error: ApiError;
}

export const ApiAlertError = ({ error }: ApiAlertErrorProps) => (
  <Alert severity="error">{getErrorMessage(error)}</Alert>
);
