import { FetchBaseQueryError } from "@reduxjs/toolkit/query";
import { SerializedError } from "@reduxjs/toolkit";

interface ErrorMessage {
  error: string;
  description: string;
}

export const isFetchBaseQueryError = (
  error: unknown
): error is FetchBaseQueryError => {
  return error != null && typeof error === "object" && "status" in error;
};

export const isErrorWithMessage = (
  error: unknown
): error is { message: string } => {
  return (
    error != null &&
    typeof error === "object" &&
    "message" in error &&
    typeof (error as SerializedError).message === "string"
  );
};

export const getErrorMessage = (
  error: FetchBaseQueryError | SerializedError | unknown
) => {
  if (error) {
    if (isFetchBaseQueryError(error)) {
      const customError = error.data as ErrorMessage;
      if (customError) {
        return customError.description
          ? customError.description
          : customError.error;
      }
      return "Unknown error";
    } else if (isErrorWithMessage(error)) {
      return error.message;
    }
  }
  return "Unknown error";
};
