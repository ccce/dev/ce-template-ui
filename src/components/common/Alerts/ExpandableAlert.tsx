import { styled } from "@mui/system";
import Alert from "@mui/material/Alert";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import AlertTitle from "@mui/material/AlertTitle";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const StyledAlert = styled(Alert)(() => ({
  "& .MuiAlert-icon": {
    padding: "9px 0"
  },
  "& .MuiAlert-message": {
    flex: 1
  }
}));

const StyledAccordion = styled(Accordion)(() => ({
  width: "100%",
  background: "none",
  boxShadow: "none",
  overflow: "hidden",
  "& .MuiButtonBase-root, .MuiButtonBase-root.Mui-expanded": {
    padding: 0,
    minHeight: "auto",
    "& .MuiAccordionSummary-content": {
      margin: 0
    }
  }
}));

const StyledAccordionDetails = styled(AccordionDetails)(() => ({
  padding: "16px 16px 8px 8px"
}));

interface ExpandableAlertProps {
  severity: "error" | "warning" | "info" | "success";
  title: string;
  details: string | JSX.Element;
}

export const ExpandableAlert = ({
  severity = "info",
  title,
  details
}: ExpandableAlertProps) => (
  <StyledAlert severity={severity}>
    <StyledAccordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1-content"
        id="panel1-header"
      >
        <AlertTitle sx={{ margin: 0, fontSize: "0.875rem" }}>
          {title}
        </AlertTitle>
      </AccordionSummary>
      <StyledAccordionDetails>{details}</StyledAccordionDetails>
    </StyledAccordion>
  </StyledAlert>
);
