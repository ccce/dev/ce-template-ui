import { GitRefLink } from "@ess-ics/ce-ui-common";
import { Skeleton } from "@mui/material";
import { useGetProjectInformationQuery } from "../../../store/templateApi";
import { ApiAlertError } from "../Alerts/ApiAlertError";

interface IocGitInfoProps {
  gitTag: string;
  gitProjectId: number;
}

export const IocGitInfo = ({ gitTag, gitProjectId }: IocGitInfoProps) => {
  const {
    data: gitProject,
    isLoading,
    error
  } = useGetProjectInformationQuery({
    projectId: gitProjectId
  });

  return (
    <>
      {isLoading && (
        <Skeleton
          height={25}
          width={90}
        />
      )}
      {error && <ApiAlertError error={error} />}
      {gitProject && (
        <GitRefLink
          url={gitProject?.project_url}
          revision={gitTag}
        />
      )}
    </>
  );
};
