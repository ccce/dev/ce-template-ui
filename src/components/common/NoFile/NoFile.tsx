import { Stack, Typography } from "@mui/material";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";

interface NoFileProps {
  message: string;
}

/**
 * Component that renders an icon with a label.
 *
 * @param {string} message Message warning about missing file for example No Config File Previewed
 */
export const NoFile = ({ message }: NoFileProps) => {
  return (
    <Stack
      color="white"
      alignItems="center"
    >
      <VisibilityOffIcon />
      <Typography>{message}</Typography>
    </Stack>
  );
};
