import { array, func } from "prop-types";
import {
  List,
  ListItem,
  Checkbox,
  Typography,
  FormControlLabel
} from "@mui/material";
import { useUniqueKeys } from "@ess-ics/ce-ui-common";
import { theme } from "../../../style/Theme";

const propTypes = {
  listData: array,
  selected: array,
  onSelect: func
};

interface CheckboxListProps {
  listData: { id: string; label: string }[];
  selected: string[];
  onSelect: (selected: string[]) => void;
}

export function CheckboxList({
  listData,
  selected,
  onSelect
}: CheckboxListProps) {
  const iocKeys = useUniqueKeys(listData);
  const toggleCheckbox = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.checked) {
      onSelect([...selected, e.target.name]);
    } else {
      onSelect(selected.filter((item) => item !== e.target.name));
    }
  };

  const toggleSelectAll = () => {
    if (selected.length === listData.length) {
      onSelect([]);
    } else {
      onSelect(listData.map((item) => item.id));
    }
  };

  return (
    <List>
      <ListItem
        sx={{
          paddingLeft: 0,
          paddingRight: 0,
          width: "100%",
          alignItems: "center",
          justifyContent: "space-between"
        }}
      >
        <FormControlLabel
          control={
            <Checkbox
              id="select-all-label"
              checked={selected.length === listData.length}
              onChange={toggleSelectAll}
            />
          }
          label={
            <Typography
              htmlFor="select-all-label"
              sx={{ fontWeight: "bold", flex: 1 }}
              component="label"
            >
              Select All
            </Typography>
          }
        />
      </ListItem>

      {listData.map((ioc, index) => {
        return (
          <ListItem
            key={iocKeys[index]}
            sx={{
              borderTop: `1px solid ${theme.palette.primary.borderColor}`,
              "&:hover": {
                backgroundColor: theme.palette.primary.hoverBackground
              }
            }}
          >
            <FormControlLabel
              control={
                <Checkbox
                  id={`select-item-${ioc.id}`}
                  name={ioc.id}
                  checked={selected.includes(ioc.id)}
                  onChange={toggleCheckbox}
                />
              }
              label={
                <Typography
                  htmlFor={`select-item-${ioc.id}`}
                  sx={{ fontWeight: "bold" }}
                  component="label"
                >
                  {ioc.label}
                </Typography>
              }
            />
          </ListItem>
        );
      })}
    </List>
  );
}
CheckboxList.propTypes = propTypes;
