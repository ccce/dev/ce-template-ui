import { useCallback } from "react";
import { SnackbarKey, useSnackbar } from "notistack";
import { Button, styled } from "@mui/material";

// eslint-disable-next-line react-refresh/only-export-components
const SnackbarButton = styled(Button)(() => ({
  color: "#f0f0f0",
  borderColor: "#f0f0f0",
  "&:hover": {
    backgroundColor: "#8d8d8d",
    borderColor: "#8d8d8d",
    boxShadow: "none"
  }
}));

export function useCustomSnackbar() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const showError = useCallback(
    (message: string) => {
      if (message) {
        const action = (key: SnackbarKey | undefined) => (
          <SnackbarButton
            variant="text"
            onClick={() => {
              closeSnackbar(key);
            }}
          >
            Dismiss
          </SnackbarButton>
        );
        enqueueSnackbar(message, {
          variant: "error",
          autoHideDuration: null,
          action
        });
      }
    },
    [enqueueSnackbar, closeSnackbar]
  );

  const showSuccess = useCallback(
    (message: string) => {
      if (message) {
        enqueueSnackbar(message, {
          variant: "success",
          autoHideDuration: 3000
        });
      }
    },
    [enqueueSnackbar]
  );

  return { showSuccess, showError };
}
