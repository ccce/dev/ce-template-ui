import { env } from "../../config/env.js";

export const applicationSubTitle = () => {
  const title = `${env.ENVIRONMENT_TITLE}`;

  if (title && title !== "undefined") {
    return ` - ${title}`;
  }

  return "";
};
