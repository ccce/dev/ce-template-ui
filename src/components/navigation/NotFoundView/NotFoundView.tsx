/**
 * NotFound
 * when page not found go (redirect) home ("/")
 */
import { string } from "prop-types";
import {
  ServerErrorPage,
  SidewaysMascot,
  VerticalMascot
} from "@ess-ics/ce-ui-common";
import { RootPaper } from "../../common/container/RootPaper";

const propTypes = {
  /** String containing message of page not found. Otherwise defaults to a generic "Page Not Found" message */
  message: string
};

interface NotFoundViewProps {
  message?: string;
}

const mascotDefaultProps = { width: "250px", height: "500px" };
const errorPageProps = {
  mascotVariantL: <VerticalMascot {...mascotDefaultProps} />,
  mascotVariantR: <SidewaysMascot {...mascotDefaultProps} />
};
export const NotFoundView = ({ message }: NotFoundViewProps) => {
  return (
    <RootPaper>
      <ServerErrorPage
        status={"404"}
        message={message}
        supportHref={window.SUPPORT_URL}
        errorPageProps={errorPageProps}
      />
    </RootPaper>
  );
};

NotFoundView.propTypes = propTypes;
