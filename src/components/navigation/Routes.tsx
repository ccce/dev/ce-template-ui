import { Navigate } from "react-router-dom";
import { NotFoundView } from "../../components/navigation/NotFoundView";
import { GenerateView } from "../../views/generate/GenerateView";
import { BrowseTemplatesView } from "../../views/templates/BrowseTemplatesView";
import { TemplateDetailsView } from "../../views/templates/TemplateDetailsView";
import { JobListView } from "../../views/job/JobListView";
import { JobDetailsView } from "../../views/job/JobDetailsView/JobDetailsView";
import { HelpView } from "../../views/help/HelpView";
import { CreateTemplateView } from "../../views/create/CreateTemplateView";

export enum PATHS {
  PATH_BASE = "/",
  PATH_CREATE = "/create",
  PATH_GENERATE = "/generate",
  PATH_TYPES = "/types",
  PATH_JOBS = "/jobs",
  PATH_JOBS_PARAM = "/jobs/:jobId",
  PATH_TYPES_PARAM = "/types/:typeId",
  PATH_HELP = "/help",
  PATH_NOT_FOUND = "*"
}

export const routes = [
  {
    path: PATHS.PATH_BASE,
    Component: () => <Navigate to={PATHS.PATH_TYPES} />
  },
  {
    path: PATHS.PATH_CREATE,
    Component: () => <CreateTemplateView />
  },
  {
    path: PATHS.PATH_GENERATE,
    Component: () => <GenerateView />
  },
  {
    path: PATHS.PATH_TYPES,
    Component: () => <BrowseTemplatesView />
  },
  {
    path: PATHS.PATH_JOBS,
    Component: () => <JobListView />
  },
  {
    path: PATHS.PATH_JOBS_PARAM,
    Component: () => <JobDetailsView />
  },
  {
    path: PATHS.PATH_TYPES_PARAM,
    Component: () => <TemplateDetailsView />
  },
  {
    path: PATHS.PATH_HELP,
    Component: () => <HelpView />
  },
  {
    path: PATHS.PATH_NOT_FOUND,
    Component: () => <NotFoundView />
  }
];
