import { Button, Avatar, Chip } from "@mui/material";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import { IconMenuButton } from "@ess-ics/ce-ui-common";
import { useAuthentication } from "../../../hooks/useAuthentication";

export function ProfileMenu() {
  const { user, logout } = useAuthentication();
  const userRoles = user.roles || [];

  const objUserRoles = userRoles
    ?.filter((role) => role.includes("CE_TemplatingTool"))
    ?.map((role) => ({
      text: (
        <Chip
          label={role}
          color="primary"
        />
      )
    }));

  const profileMenuProps = {
    /*
      If editing this id, be sure to edit the check for this in
      IconMenuButton.js in ce-ui-common repo as well to
      match
    */
    id: "user-login-profile-",
    icon: (
      <Avatar
        alt={user.fullName}
        src={user.avatar}
        sx={{
          width: (theme) => theme.spacing(3),
          height: (theme) => theme.spacing(3)
        }}
      />
    ),
    menuItems: [
      ...(objUserRoles || []),
      {
        text: "Logout",
        action: () => logout(),
        icon: <LockOpenIcon fontSize="small" />
      }
    ]
  };

  return <IconMenuButton {...profileMenuProps} />;
}

export function LoginControls() {
  const { isLoggedIn, login } = useAuthentication();

  return isLoggedIn ? (
    <ProfileMenu />
  ) : (
    <>
      <Button
        color="inherit"
        onClick={() => login()}
      >
        Login
      </Button>
    </>
  );
}
