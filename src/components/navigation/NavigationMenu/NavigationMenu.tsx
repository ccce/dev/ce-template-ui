/**
 * NavigationMenu
 * Component containing menu items for
 * the left drawer
 */
import { useState, Fragment } from "react";
import { List, Assignment, HelpCenter } from "@mui/icons-material";
import {
  Box,
  Divider,
  IconButton,
  ListItemIcon,
  ListItemText,
  ListItemButton,
  Tooltip
} from "@mui/material";
import {
  GlobalAppBar,
  EssIconLogo,
  IconMenuButton,
  useUniqueKeys
} from "@ess-ics/ce-ui-common";
import { Link, useNavigate } from "react-router-dom";
import {
  string,
  object,
  array,
  bool,
  node,
  oneOfType,
  arrayOf
} from "prop-types";
import { LoginControls } from "./LoginControls";
import { PATHS } from "../Routes";
import { useAuthentication } from "../../../hooks/useAuthentication";

const propTypes = {
  menuListItem: {
    /** String containing menu item url */
    url: string,
    /** Object containing menu item icon */
    icon: object,
    /** String containing menu item text */
    text: string,
    /** Boolean value, whether to show tooltip or not */
    tooltip: bool
  },
  menuListItems: {
    /** Object containing menu items */
    menuItems: array,
    /** Boolean value, whether to set drawer as open or closed */
    drawerOpen: bool
  },
  childrenPropTypes: {
    /** Elements used as children */
    children: oneOfType([
      /** Array of elements. */
      arrayOf(node),
      /** One or more elements without array. */
      node
    ])
  }
};

interface MenuListItemProps {
  url: string;
  icon: React.ReactElement;
  text: string;
  tooltip: boolean;
}
function MenuListItem({ url, icon, text, tooltip }: MenuListItemProps) {
  const currentUrl = `${window.location}`;

  return (
    <Tooltip
      title={tooltip ? text : ""}
      placement="right"
      arrow
    >
      <ListItemButton
        component={Link}
        to={url}
        selected={currentUrl.split("?")[0].endsWith(url)}
      >
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText
          primary={text}
          primaryTypographyProps={{ variant: "button" }}
        />
      </ListItemButton>
    </Tooltip>
  );
}

interface MenuLink {
  text: string;
  url: string;
  icon: React.ReactElement;
}
interface MenuListItemsProps {
  menuItems: MenuLink[];
  drawerOpen: boolean;
}
function MenuListItems({ menuItems, drawerOpen }: MenuListItemsProps) {
  const menuItemsKeys = useUniqueKeys(menuItems);
  return (
    <Box sx={{ paddingTop: 3 }}>
      {menuItems.map(({ text, url, icon }, i) => (
        <Fragment key={menuItemsKeys[i]}>
          {typeof (text && url && icon) === "undefined" ? (
            <Divider sx={{ marginTop: 5 }} />
          ) : (
            <MenuListItem
              tooltip={!drawerOpen}
              url={url}
              icon={icon}
              text={text}
            />
          )}
        </Fragment>
      ))}
    </Box>
  );
}

const makeLink = (
  text: string,
  url: string,
  icon: React.ReactElement
): MenuLink => ({ text, url, icon });

const menuItemsAll = [
  makeLink("IOC types", "/types", <List />),
  makeLink("Job log", "/jobs", <Assignment />)
];

const NavIconMenuButton = () => {
  const { isLoggedIn } = useAuthentication();
  const navigate = useNavigate();

  const iconMenuButtonProps = {
    menuItems: [
      {
        text: "New IOC Type",
        action: () => navigate(PATHS.PATH_CREATE)
      }
    ]
  };

  return isLoggedIn ? <IconMenuButton {...iconMenuButtonProps} /> : null;
};

interface NavigationMenuProps {
  children: React.ReactNode;
}
export function NavigationMenu({ children }: NavigationMenuProps) {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const navigate = useNavigate();
  const goHome = () => {
    navigate(PATHS.PATH_BASE);
  };

  const helpButtonProps = {
    icon: <HelpCenter />,
    menuItems: [
      {
        text: "Help",
        action: () => navigate(PATHS.PATH_HELP)
      }
    ]
  };

  const args = {
    defaultHomeButton: (
      <IconButton
        edge="start"
        color="inherit"
        onClick={goHome}
      >
        <EssIconLogo />
      </IconButton>
    ),
    defaultTitle: "CE template",
    defaultActionButton: <LoginControls />,
    defaultOpen: false,
    widthOpen: "241px",
    widthClosed: "57px",
    defaultIconMenuButton: <NavIconMenuButton />,
    defaultHelpButton: <IconMenuButton {...helpButtonProps} />
  };

  return (
    <GlobalAppBar
      getDrawerOpen={setDrawerOpen}
      menuItems={
        <MenuListItems
          drawerOpen={drawerOpen}
          menuItems={menuItemsAll}
        />
      }
      {...args}
    >
      {children}
    </GlobalAppBar>
  );
}

MenuListItem.propTypes = propTypes.menuListItem;
MenuListItems.propTypes = propTypes.menuListItems;
NavigationMenu.propTypes = propTypes.childrenPropTypes;
