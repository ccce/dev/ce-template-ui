import { LabeledIcon } from "@ess-ics/ce-ui-common";
import { JOB_TYPE_ICONS } from "../JobDetails/JobDetailsData";
import { JobDetails } from "../../store/templateApi";

interface JobIconWithLabelProps {
  jobType: JobDetails["job_type"];
}

export const JobIconWithLabel = ({ jobType }: JobIconWithLabelProps) => (
  <LabeledIcon
    label={JOB_TYPE_ICONS[jobType].label}
    LabelProps={{ component: "p" }}
    labelPosition="right"
    Icon={JOB_TYPE_ICONS[jobType].icon}
  />
);
