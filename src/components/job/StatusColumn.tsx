import { StartAndDuration } from "@ess-ics/ce-ui-common";
import { Stack } from "@mui/material";
import { JobStatus } from "./JobStatus";
import { Job } from "../../store/templateApi";

interface StatusColumnProps {
  job: Job;
}

export const StatusColumn = ({ job }: StatusColumnProps) => (
  <Stack
    direction="column"
    gap={0.5}
  >
    <JobStatus status={job.status} />
    <StartAndDuration
      createdAt={job.created_at && new Date(job.created_at)}
      finishedAt={job.finished_at && new Date(job.finished_at)}
    />
  </Stack>
);
