import {
  STEPPER_STATES,
  LabeledIcon,
  IconRunning,
  IconSuccessful,
  IconFailed,
  IconUnknown
} from "@ess-ics/ce-ui-common";
import { theme } from "../../style/Theme";
import { Job, JobStatusResponse } from "../../store/templateApi";

interface JobStatusProps {
  status: Job["status"] | JobStatusResponse["status"];
}

const getStatus = ({ status }: JobStatusProps) => {
  switch (status) {
    case STEPPER_STATES.QUEUED:
      return {
        label: "Queued",
        icon: IconRunning
      };
    case STEPPER_STATES.RUNNING:
      return {
        label: "Running",
        icon: IconRunning
      };
    case STEPPER_STATES.SUCCESSFUL:
      return {
        label: "Successful",
        icon: IconSuccessful
      };
    case STEPPER_STATES.FAILED:
      return {
        label: "Failed",
        icon: IconFailed
      };
    case STEPPER_STATES.PARTIAL_FAILURE:
      return {
        label: "Partial failures",
        icon: IconFailed
      };
    default:
      return {
        label: "Unknown",
        icon: IconUnknown
      };
  }
};

export const JobStatus = (status: JobStatusProps) => {
  const activeStep = getStatus(status);
  const Icon = activeStep.icon;

  return (
    <LabeledIcon
      label={activeStep.label}
      labelPosition="right"
      LabelProps={{
        color:
          status === STEPPER_STATES.FAILED ||
          status === STEPPER_STATES.PARTIAL_FAILURE
            ? theme.palette.status.fail.main
            : "inherit"
      }}
      Icon={() => <Icon size="medium" />}
    />
  );
};
