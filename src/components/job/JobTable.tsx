import { useEffect } from "react";
import { usePagination, Table } from "@ess-ics/ce-ui-common";
import { StatusColumn } from "./StatusColumn";
import { JobColumn } from "./JobColumn";
import { DEFAULT_POLLING_INTERVAL_MILLIS } from "../../store/apiConfig";
import { Job, useListJobsQuery } from "../../store/templateApi";
import { UserAvatar } from "../common/UserAvatar/UserAvatar";
import { ROWS_PER_PAGE } from "../../constants";

const columns = [
  { field: "status", headerName: "Status" },
  { field: "job", headerName: "Job" },
  {
    field: "instance_count",
    headerName: "Count",
    align: "right",
    headerAlign: "right",
    maxWidth: 80
  },
  {
    field: "created_by",
    headerName: "User",
    align: "center",
    headerAlign: "center"
  }
];

const createRow = (job: Job) => ({
  id: job.id,
  status: <StatusColumn job={job} />,
  job: <JobColumn job={job} />,
  instance_count: job.instance_count,
  created_by: <UserAvatar createdBy={job.created_by || ""} />
});

interface JobTableProps {
  templateId?: number | undefined;
}

export function JobTable({ templateId = undefined }: JobTableProps) {
  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0]
  });

  const { data: jobList, isLoading } = useListJobsQuery(
    {
      iocTypeId: templateId,
      ...pagination
    },
    { pollingInterval: DEFAULT_POLLING_INTERVAL_MILLIS }
  );

  const params = {
    columns,
    rows: jobList?.jobs?.map((job: Job) => createRow(job)) || [],
    loading: isLoading,
    pagination,
    onPage: setPagination,
    rowHeight: 90
  };

  useEffect(
    () => setPagination({ totalCount: jobList?.total_count ?? 0 }),
    [setPagination, jobList?.total_count]
  );

  return <Table {...params} />;
}
