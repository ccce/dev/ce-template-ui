import { Chip, Stack } from "@mui/material";
import { ExternalLink } from "@ess-ics/ce-ui-common";
import LabelOutlinedIcon from "@mui/icons-material/LabelOutlined";
import QuestionMarkIcon from "@mui/icons-material/QuestionMark";
import CommitIcon from "@mui/icons-material/Commit";
import { GitProjectInfo } from "../../store/templateApi";

interface JobRevisionChipProps {
  revision: GitProjectInfo | undefined;
}

const REFERENCE_TYPE_ICONS = {
  COMMIT: CommitIcon,
  TAG: LabelOutlinedIcon,
  UNKNOWN: QuestionMarkIcon
};

export const JobRevisionChip = ({ revision }: JobRevisionChipProps) => {
  const Icon = REFERENCE_TYPE_ICONS[revision?.reference_type || "UNKNOWN"];

  return (
    <Chip
      size="small"
      label={
        <Stack
          flexDirection="row"
          gap={0.5}
          sx={{ marginTop: "2px" }}
        >
          <Icon fontSize="small" />
          <ExternalLink
            href={revision?.reference_url}
            label="Type Revision"
            disableExternalLinkIcon
          >
            {revision?.reference}
          </ExternalLink>
        </Stack>
      }
    />
  );
};
