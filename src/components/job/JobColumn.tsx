import { InternalLink } from "@ess-ics/ce-ui-common";
import { Stack, Chip } from "@mui/material";
import LabelOutlinedIcon from "@mui/icons-material/LabelOutlined";
import { JobIconWithLabel } from "./JobTypes";
import { Job } from "../../store/templateApi";

interface JobColumnProps {
  job: Job;
}

export const JobColumn = ({ job }: JobColumnProps) => (
  <Stack direction="column">
    <JobIconWithLabel jobType={job.job_type} />
    <InternalLink to={`/types/${job.ioc_type_id}`}>
      {job.ioc_type_name}
    </InternalLink>
    <Stack
      direction="row"
      gap={0.5}
    >
      <InternalLink to={`/jobs/${job.id}`}>#{job.id}</InternalLink>
      {job.tag_name && (
        <Chip
          size="small"
          icon={<LabelOutlinedIcon color="inherit" />}
          label={job.tag_name}
        />
      )}
    </Stack>
  </Stack>
);
