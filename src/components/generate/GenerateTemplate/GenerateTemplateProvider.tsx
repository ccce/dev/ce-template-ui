import {
  useEffect,
  useMemo,
  useState,
  ReactNode,
  Dispatch,
  SetStateAction
} from "react";
import {
  defaultValue,
  GenerateTemplateContextInstance
} from "./GenerateTemplateContext";
import { Type, ConfigFile } from "../../../store/templateApi";

interface SelectedState {
  configs?: ConfigFile[];
  configPreview?: ConfigFile;
  templateReference: string;
  configReference: string;
  selectedAll: boolean;
}

export interface GenerateTemplateContext {
  type: Type | undefined;
  setType: Dispatch<SetStateAction<Type | undefined>>;
  selectedState: SelectedState;
  setSelectedState: Dispatch<SetStateAction<SelectedState>>;
  ignoredFiles: string[];
  setIgnoredFiles: Dispatch<SetStateAction<string[]>>;
  drawerOpen: boolean;
  setDrawerOpen: (drawerOpen: boolean) => void;
}

interface GenerateTemplateProviderProps {
  children: ReactNode;
}

export const GenerateTemplateProvider = ({
  children
}: GenerateTemplateProviderProps) => {
  const [type, setType] = useState(defaultValue.type);
  const [selectedState, setSelectedState] = useState<SelectedState>(
    defaultValue.selectedState
  );

  const { templateReference, configReference } = selectedState;

  const [ignoredFiles, setIgnoredFiles] = useState(defaultValue.ignoredFiles);
  const [drawerOpen, setDrawerOpen] = useState(defaultValue.drawerOpen);

  useEffect(() => {
    setSelectedState((prevState) => ({
      ...prevState,
      configReference: ""
    }));
  }, [templateReference]);

  useEffect(() => {
    resetConfigSelector();
    setDrawerOpen(!!configReference);
  }, [configReference]);

  useEffect(() => {
    /*
      This overrides the body container style in GlobalAppBar from @ess-common.
      The body container implementation does not allow for a right side drawer
    */
    const elem = Array.from(
      document.getElementsByClassName(
        "CeuiGlobalAppBar-ContentBox"
      ) as HTMLCollectionOf<HTMLElement>
    )[0];
    elem.style.padding = "24px 0 24px 24px";
    elem.style.minWidth = "0";
    elem.style.transition = "padding 0.2s";

    if (drawerOpen) {
      elem.style.paddingRight = "324px";
    } else {
      elem.style.paddingRight = "80px";
    }

    return () => {
      elem.style.paddingRight = "24px";
    };
  }, [drawerOpen]);

  const resetConfigSelector = () => {
    setSelectedState((prevState) => ({
      ...prevState,
      configs: undefined,
      configPreview: undefined,
      selectedAll: false
    }));
  };

  const value = useMemo(
    () => ({
      type,
      setType,
      selectedState,
      setSelectedState,
      ignoredFiles,
      setIgnoredFiles,
      drawerOpen,
      setDrawerOpen
    }),
    [type, selectedState, ignoredFiles, drawerOpen]
  );

  return (
    <GenerateTemplateContextInstance.Provider value={value}>
      {children}
    </GenerateTemplateContextInstance.Provider>
  );
};
