import { ReactNode } from "react";
import { ChevronLeft, ChevronRight } from "@mui/icons-material";
import { IconButton, Stack, Tooltip } from "@mui/material";

interface ToggleOpenButtonProps {
  children?: ReactNode;
  open: boolean;
  toggleBtnDisabled: boolean;
  toggleOpen: () => void;
}

export const ToggleOpenButton = ({
  open,
  toggleBtnDisabled,
  toggleOpen
}: ToggleOpenButtonProps) => (
  <Stack>
    <Tooltip
      title={open ? "Hide instance(s)" : "Show instance(s)"}
      placement="right"
      arrow
    >
      <span>
        <IconButton
          disabled={toggleBtnDisabled}
          onClick={toggleOpen}
          aria-label={
            open
              ? "Close select instance(s) drawer"
              : "Open select instance(s) drawer"
          }
          sx={{ margin: "10px 10px 10px 0" }}
        >
          {open ? <ChevronRight /> : <ChevronLeft />}
        </IconButton>
      </span>
    </Tooltip>
  </Stack>
);
