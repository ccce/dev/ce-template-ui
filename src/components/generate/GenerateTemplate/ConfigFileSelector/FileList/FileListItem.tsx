import { Fragment } from "react";
import {
  ListItemButton,
  ListItemText,
  ListItemIcon,
  IconButton,
  Checkbox,
  Box,
  Tooltip
} from "@mui/material";
import { Preview, ErrorOutline } from "@mui/icons-material";
import { useUniqueKeys } from "@ess-ics/ce-ui-common";
import { StyledListItem } from "./StyledListItem";
import { theme } from "../../../../../style/Theme";
import { ConfigFile } from "../../../../../store/templateApi";
import { useGenerateTemplateContext } from "../../GenerateTemplateContext";

interface PreviewButtonProps {
  file: ConfigFile;
  isPreviewed: boolean;
}

const PreviewButton = ({ file, isPreviewed }: PreviewButtonProps) => {
  const valid = file?.is_valid;
  const fileMessages = file?.error_message?.split(",") ?? [];
  const fileMessagesKeys = useUniqueKeys(fileMessages);
  const { setSelectedState } = useGenerateTemplateContext();

  return (
    <Box
      sx={{
        width: "100%",
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "center"
      }}
    >
      {!valid && (
        <Tooltip
          placement="top-start"
          title={fileMessages.map((message, i) => (
            <Fragment key={fileMessagesKeys[i]}>
              {message}
              <br />
            </Fragment>
          ))}
        >
          <ErrorOutline sx={{ fill: theme.palette.primary.inconsistency }} />
        </Tooltip>
      )}
      <IconButton
        color="inherit"
        sx={isPreviewed ? { backgroundColor: "#cccccc" } : undefined}
        edge="end"
        aria-label={`preview file ${file}${isPreviewed ? ", previewed" : ""}`}
        onClick={() =>
          setSelectedState((prevState) => ({
            ...prevState,
            configPreview: prevState?.configPreview === file ? undefined : file
          }))
        }
      >
        <Preview />
      </IconButton>
    </Box>
  );
};

interface FileListItemProps {
  file: ConfigFile;
  isPreviewed: boolean;
  isSelected: boolean;
  onSelectConfig: (file: ConfigFile, select: boolean) => void;
}

export const FileListItem = ({
  file,
  isPreviewed,
  isSelected,
  onSelectConfig
}: FileListItemProps) => {
  const id = `checkbox-list-secondary-label-${file?.file_name}`;
  const isValid = file?.is_valid;

  return (
    <StyledListItem
      secondaryAction={
        <PreviewButton
          file={file}
          isPreviewed={isPreviewed}
        />
      }
      disablePadding
    >
      <ListItemButton
        onClick={() => onSelectConfig(file, !isSelected)}
        disabled={!isValid}
      >
        <ListItemIcon>
          <Checkbox
            edge="start"
            checked={isSelected}
            inputProps={{ "aria-labelledby": id }}
            disabled={!isValid}
          />
        </ListItemIcon>
        <ListItemText
          id={id}
          primary={file?.file_name}
        />
      </ListItemButton>
    </StyledListItem>
  );
};
