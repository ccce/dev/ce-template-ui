/**
 * FileList
 * Containing a list of selection of configuration files revision
 */
import { useUniqueKeys } from "@ess-ics/ce-ui-common";
import {
  styled,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  ListItemIcon,
  Checkbox
} from "@mui/material";
import { FileListItem } from "./FileListItem";
import { ConfigFile } from "../../../../../store/templateApi";
import { useGenerateTemplateContext } from "../../GenerateTemplateContext";

interface FileListProps {
  configFiles: ConfigFile[];
}

const StyledListItem = styled(ListItem)(({ theme }) => ({
  position: "sticky",
  top: 0,
  zIndex: 9,
  backgroundColor: "white",
  borderBottom: `1px solid ${theme.palette.primary.borderColor}`,
  "&.MuiListItem-root:hover": {
    backgroundColor: theme.palette.primary.hoverBackground
  }
}));

export function FileList({ configFiles }: FileListProps) {
  const {
    selectedState: { configs, configPreview, selectedAll },
    setSelectedState
  } = useGenerateTemplateContext();

  const filesKeys = useUniqueKeys(configFiles);
  const filteredFiles = configFiles.filter((file) => file.is_valid);

  const toggleSelectedAll = () => {
    setSelectedState((prevState) => ({
      ...prevState,
      configs: selectedAll ? undefined : filteredFiles,
      selectedAll: !selectedAll
    }));
  };

  const onSelectConfig = (file: ConfigFile, select: boolean) => {
    const copyConfigs = configs
      ? new Set<ConfigFile>([...configs])
      : new Set<ConfigFile>();

    if (select) {
      copyConfigs.add(file);
    } else {
      copyConfigs.delete(file);
    }

    setSelectedState((prevState) => ({
      ...prevState,
      configs: Array.from(copyConfigs),
      selectedAll: copyConfigs.size === filteredFiles.length
    }));
  };

  return (
    <List sx={{ paddingBottom: "60px", paddingTop: "0" }}>
      <StyledListItem disablePadding>
        <ListItemButton
          sx={{ padding: "12px 16px" }}
          onClick={toggleSelectedAll}
        >
          <ListItemIcon>
            <Checkbox
              edge="start"
              onChange={toggleSelectedAll}
              checked={selectedAll}
              inputProps={{
                "aria-labelledby": "checkbox-list-secondary-label-select-all"
              }}
            />
          </ListItemIcon>
          <ListItemText
            id="checkbox-list-secondary-label-select-all"
            primary="Select all"
            primaryTypographyProps={{ fontWeight: "bold" }}
          />
        </ListItemButton>
      </StyledListItem>
      {configFiles?.map((file, i) => (
        <FileListItem
          key={filesKeys[i]}
          file={file}
          isPreviewed={file?.file_name === configPreview?.file_name}
          isSelected={Boolean(configs?.includes(file))}
          onSelectConfig={onSelectConfig}
        />
      ))}
    </List>
  );
}
