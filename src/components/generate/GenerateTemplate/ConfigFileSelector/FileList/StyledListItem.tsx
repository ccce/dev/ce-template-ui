import { ListItem, styled } from "@mui/material";

export const StyledListItem = styled(ListItem)(({ theme }) => ({
  borderBottom: `1px solid ${theme.palette.primary.borderColor}`,
  "&.MuiListItem-root:hover": {
    backgroundColor: theme.palette.primary.hoverBackground
  }
}));
