import {
  Drawer,
  Stack,
  Box,
  Button,
  styled,
  LinearProgress,
  Typography
} from "@mui/material";
import GradingIcon from "@mui/icons-material/Grading";
import { useState } from "react";
import { skipToken } from "@reduxjs/toolkit/query";
import { ToggleOpenButton } from "./ToggleOpenButton";
import { FileList } from "./FileList";
import { useGetConfigFilesQuery } from "../../../../store/templateApi";
import { ApiAlertError } from "../../../common/Alerts/ApiAlertError";
import { useGenerateTemplateContext } from "../GenerateTemplateContext";
import { GenerateTemplateConfirmationDialog } from "../GenerateTemplateConfirmationDialog";

export const ToggleBox = styled(Box, {
  shouldForwardProp: (props) => props !== "drawerOpen"
})<{ drawerOpen: boolean }>(({ drawerOpen }) => ({
  display: "flex",
  justifyContent: "flex-end",
  alignItems: "center",
  transition: "width 0.2s",
  overflow: "hidden",
  position: "fixed",
  bottom: 0,
  width: drawerOpen ? "300px" : "56px",
  background: "white",
  borderTop: drawerOpen ? "1px solid #e0e0e0" : "none",
  zIndex: 99
}));

export const ConfigFileSelectorDrawer = () => {
  const {
    type,
    selectedState: { configs, configReference, templateReference },
    drawerOpen,
    setDrawerOpen
  } = useGenerateTemplateContext();
  const [confirmationOpen, setConfirmationOpen] = useState(false);
  const { id: typeId } = type || {};
  const selectedConfigCount =
    configs && configs?.length > 0 ? `(${configs?.length})` : null;
  const requestReady = !!configs && configs.length > 0;

  const {
    data: configFiles,
    isLoading: configFilesLoading,
    isFetching: configFilesFetching,
    error: configFilesError
  } = useGetConfigFilesQuery(
    typeId && configReference && templateReference
      ? {
          typeId,
          typeRevision: templateReference,
          configRevision: configReference
        }
      : skipToken
  );

  return (
    <Drawer
      sx={{
        "& > .MuiPaper-root": {
          overscrollBehavior: "contain",
          paddingTop: "64px"
        }
      }}
      anchor="right"
      open={drawerOpen}
      variant="permanent"
    >
      <Stack
        justifyContent="space-between"
        width={drawerOpen ? "300px" : "56px"}
        sx={{
          transition: "width 0.2s",
          position: "relative"
        }}
      >
        {drawerOpen ? (
          <>
            {(configFilesLoading || configFilesFetching) && (
              <Box sx={{ padding: "40px 16px" }}>
                <LinearProgress />
              </Box>
            )}
            {configFilesError && (
              <Box
                px={2}
                pt={3}
              >
                <ApiAlertError error={configFilesError} />
              </Box>
            )}
            {configFiles && !configFilesFetching && (
              <>
                {configFiles && configFiles.length > 0 ? (
                  <FileList configFiles={configFiles} />
                ) : (
                  <Typography
                    variant="body1"
                    p={2}
                    pt={4}
                  >
                    No configuration files found
                  </Typography>
                )}
              </>
            )}
          </>
        ) : (
          <GradingIcon
            color="action"
            fontSize="large"
            sx={{ margin: "40px auto 20px" }}
          />
        )}

        <ToggleBox drawerOpen={drawerOpen}>
          <Button
            sx={{
              display: drawerOpen ? "block" : "none",
              width: "100%",
              margin: "0 20px"
            }}
            variant="contained"
            disabled={!requestReady}
            onClick={() => setConfirmationOpen(true)}
          >
            Generate {selectedConfigCount}
          </Button>

          <ToggleOpenButton
            open={drawerOpen}
            toggleBtnDisabled={!configReference}
            toggleOpen={() => setDrawerOpen(!drawerOpen)}
          />
        </ToggleBox>

        {confirmationOpen && (
          <GenerateTemplateConfirmationDialog
            open={confirmationOpen}
            onClose={() => setConfirmationOpen(false)}
          />
        )}
      </Stack>
    </Drawer>
  );
};
