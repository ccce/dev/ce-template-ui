import { useEffect } from "react";
import { Box, Stack, Typography } from "@mui/material";
import { RootPaper } from "@ess-ics/ce-ui-common";
import { RevisionsSelector } from "./RevisionSelector/RevisionsSelector";
import { FilePreviewPane } from "./FilePreviewPane/FilePreviewPane";
import { useGenerateTemplateContext } from "./GenerateTemplateContext";
import { ConfigFileSelectorDrawer } from "./ConfigFileSelector/ConfigFileSelectorDrawer";
import { ExpandableAlert } from "../../common/Alerts/ExpandableAlert";
import { Type } from "../../../store/templateApi";

export const GenerateTemplate = ({ type }: { type: Type }) => {
  const {
    setType,
    selectedState: { configPreview, templateReference, configReference }
  } = useGenerateTemplateContext();

  useEffect(() => {
    setType(type);
  }, [setType, type]);

  return (
    <Stack
      alignItems="center"
      sx={{
        width: "100%",
        transition: "padding 0.2s"
      }}
    >
      <RootPaper>
        <RevisionsSelector />
        {templateReference && configReference && (
          <>
            <Typography
              paddingY="20px"
              variant="h3"
              component="h2"
            >
              Preview
            </Typography>

            <FilePreviewPane />

            {configPreview?.error_message && (
              <Box marginTop="10px">
                <ExpandableAlert
                  severity="error"
                  title="Validation error(s)"
                  details={configPreview?.error_message ?? ""}
                />
              </Box>
            )}
          </>
        )}
      </RootPaper>
      <ConfigFileSelectorDrawer />
    </Stack>
  );
};
