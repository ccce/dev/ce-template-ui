import { Dialog, useUniqueKeys } from "@ess-ics/ce-ui-common";
import {
  Alert,
  LinearProgress,
  Typography,
  Button,
  Stack,
  Box
} from "@mui/material";
import { useEffect, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { useGenerateTemplateContext } from "./GenerateTemplateContext";
import { PATHS } from "../../navigation/Routes";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";
import { ExpandableAlert } from "../../common/Alerts/ExpandableAlert";
import {
  useCalculateNewIocsMutation,
  useCreateIocRevisionMutation
} from "../../../store/templateApi";

interface GenerateTemplateConfirmationDialogProps {
  open: boolean;
  onClose: () => void;
}

export const GenerateTemplateConfirmationDialog = ({
  open,
  onClose
}: GenerateTemplateConfirmationDialogProps) => {
  const navigate = useNavigate();

  const {
    type,
    selectedState: { configs, templateReference, configReference },
    ignoredFiles
  } = useGenerateTemplateContext();

  const { id: typeId } = type || {};

  const selectedConfigsNames = useMemo(
    () => configs?.map((config) => config.file_name || "") || [],
    [configs]
  );

  const ignoredFilesKeys = useUniqueKeys(ignoredFiles);

  const [
    calculateNewIocs,
    {
      data: calculatedIocCount,
      isLoading: calculatedIocCountLoading,
      error: calculatedIocCountError,
      reset: resetCalculatedIocs
    }
  ] = useCalculateNewIocsMutation();

  const [
    createIocRevision,
    {
      isLoading: createIocRevisionLoading,
      error: createIocRevisionError,
      data: createIocRevisionResponse,
      reset: resetCreateIocRevision
    }
  ] = useCreateIocRevisionMutation();

  useEffect(() => {
    if (createIocRevisionResponse) {
      navigate(`${PATHS.PATH_JOBS}/${createIocRevisionResponse.operation_id}`);
    }
  }, [createIocRevisionResponse, navigate]);

  useEffect(() => {
    if (typeId && selectedConfigsNames?.length > 0) {
      calculateNewIocs({
        typeId: typeId,
        calculateNewIocsRequest: {
          config_names: selectedConfigsNames
        }
      });
    }
  }, [selectedConfigsNames, calculateNewIocs, typeId]);

  const onConfirm = () => {
    if (typeId) {
      createIocRevision({
        typeId: typeId,
        processTypeRequest: {
          type_revision: templateReference,
          config_revision: configReference,
          config_names: selectedConfigsNames
        }
      });
    }
  };

  return (
    <Dialog
      title="Confirm generation"
      confirmText="Generate"
      cancelText="Cancel"
      content={
        <Stack>
          <Stack gap={1}>
            {calculatedIocCountLoading && (
              <Alert severity="info">
                <Typography marginBottom="8px">
                  Computing repository count...
                </Typography>
                <LinearProgress />
              </Alert>
            )}
            {ignoredFiles.length > 0 && (
              <ExpandableAlert
                severity="info"
                title="Ignored files"
                details={
                  <ul style={{ paddingLeft: "20px", margin: 0 }}>
                    {ignoredFiles.map((fileName, i) => (
                      <li
                        style={{ fontFamily: "Monospace" }}
                        key={ignoredFilesKeys[i]}
                      >
                        {fileName}
                      </li>
                    ))}
                  </ul>
                }
              />
            )}
            {calculatedIocCountError && (
              <ApiAlertError error={calculatedIocCountError} />
            )}
          </Stack>

          <Box sx={{ margin: "20px 0 40px" }}>
            <Typography>
              This will generate a new revision for{" "}
              <Typography
                component="span"
                fontWeight="bold"
              >
                {selectedConfigsNames?.length}
              </Typography>{" "}
              IOCs.
            </Typography>

            {calculatedIocCount ? (
              <Typography>
                <Typography
                  component="span"
                  fontWeight="bold"
                >
                  {calculatedIocCount?.new_ioc_count}
                </Typography>{" "}
                new IOC repositories will be created as part of this.
              </Typography>
            ) : null}
          </Box>

          {createIocRevisionLoading && (
            <Stack my="10px">
              <LinearProgress sx={{ width: "100%" }} />
            </Stack>
          )}

          <Stack>
            {createIocRevisionError && (
              <ApiAlertError error={createIocRevisionError} />
            )}
            <Button
              sx={{ marginLeft: "auto", marginTop: "10px", minWidth: "150px" }}
              color="primary"
              variant="contained"
              autoFocus
              disabled={createIocRevisionLoading}
              onClick={onConfirm}
            >
              Confirm
            </Button>
          </Stack>
        </Stack>
      }
      s
      open={open}
      onClose={() => {
        onClose();
        resetCreateIocRevision();
        resetCalculatedIocs();
      }}
    />
  );
};
