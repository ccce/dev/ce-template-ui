import { createContext, useContext } from "react";
import { GenerateTemplateContext } from "./GenerateTemplateProvider";

export const useGenerateTemplateContext = () =>
  useContext(GenerateTemplateContextInstance);

export const defaultValue: GenerateTemplateContext = {
  type: undefined,
  setType: () => {},
  selectedState: {
    configs: undefined,
    configPreview: undefined,
    templateReference: "",
    configReference: "",
    selectedAll: false
  },
  setSelectedState: () => {},
  ignoredFiles: [],
  setIgnoredFiles: () => {},
  drawerOpen: false,
  setDrawerOpen: () => {}
};

export const GenerateTemplateContextInstance =
  createContext<GenerateTemplateContext>(defaultValue);
