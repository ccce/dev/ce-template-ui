import { useEffect, useState } from "react";
import { Paper, Stack, ToggleButton, ToggleButtonGroup } from "@mui/material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import CodeIcon from "@mui/icons-material/Code";
import { skipToken } from "@reduxjs/toolkit/query";
import { FilePreview } from "./FilePreview";
import {
  useFetchFileContentQuery,
  usePreviewGenerationMutation
} from "../../../../store/templateApi";
import { NoFile } from "../../../common/NoFile";
import { useGenerateTemplateContext } from "../GenerateTemplateContext";
import { isFetchBaseQueryError } from "../../../common/Alerts/ErrorHandling";

interface TemplateFilePreviewProps {
  selectedTemplateFile: string;
}

export const TemplateFilePreview = ({
  selectedTemplateFile
}: TemplateFilePreviewProps) => {
  const {
    type,
    selectedState: { configPreview, templateReference, configReference }
  } = useGenerateTemplateContext();
  const { id: typeId, type_project_id } = type || {};

  const [isRaw, setIsRaw] = useState(true);
  const [previewContent, setPreviewContent] = useState("");

  const {
    data: templateFileContent,
    isLoading: templateFileContentIsLoading,
    error: templateFileContentError
  } = useFetchFileContentQuery(
    type_project_id
      ? {
          projectId: type_project_id,
          projectReference: templateReference,
          fileName: selectedTemplateFile
        }
      : skipToken
  );

  const [
    generatePreview,
    {
      data: generatedPreview,
      isLoading: generatedPreviewIsLoading,
      error: generatedPreviewError,
      reset: resetGeneratedPreview
    }
  ] = usePreviewGenerationMutation();
  const substitutedContent = generatedPreview?.substituted_content;
  let isInvalidJsonError = false;
  if (
    isFetchBaseQueryError(generatedPreviewError) &&
    generatedPreviewError.status === 412
  ) {
    isInvalidJsonError = true;
  }

  useEffect(() => {
    if (typeId && configPreview && selectedTemplateFile && configReference) {
      generatePreview({
        typeId,
        previewGenerationRequest: {
          type_revision: templateReference,
          template_name: selectedTemplateFile,
          config_revision: configReference,
          config_name: configPreview?.file_name
        }
      });
    }
  }, [
    configPreview,
    selectedTemplateFile,
    configReference,
    generatePreview,
    templateReference,
    typeId
  ]);

  useEffect(() => {
    setIsRaw(true);
    setPreviewContent(templateFileContent?.content || "");
  }, [configPreview, templateFileContent?.content]);

  useEffect(() => {
    if (!configPreview) {
      resetGeneratedPreview();
    }
  }, [configPreview, resetGeneratedPreview]);

  return (
    <Stack
      width="100%"
      height="500px"
      position="relative"
    >
      <Paper
        component={Stack}
        flexDirection="row"
        position="absolute"
        top={6}
        right={5}
        zIndex={20}
        gap={1}
        elevation={12}
      >
        <ToggleButtonGroup
          color="primary"
          size="small"
          aria-label="View Mode"
        >
          <ToggleButton
            value="false"
            onClick={() => {
              setIsRaw(true);
              setPreviewContent(templateFileContent?.content || "");
            }}
            selected={isRaw}
            aria-label="Raw"
          >
            <CodeIcon />
            Raw
          </ToggleButton>
          <ToggleButton
            value="true"
            onClick={() => {
              setIsRaw(false);
              setPreviewContent(substitutedContent || "");
            }}
            selected={!isRaw}
            aria-label="Substituted"
            disabled={!substitutedContent}
          >
            {substitutedContent ? <VisibilityIcon /> : <VisibilityOffIcon />}
            Substituted
          </ToggleButton>
        </ToggleButtonGroup>
      </Paper>
      <FilePreview
        value={previewContent || templateFileContent?.content}
        isLoading={templateFileContentIsLoading || generatedPreviewIsLoading}
        error={
          templateFileContentError ||
          (isInvalidJsonError ? undefined : generatedPreviewError)
        }
        renderOnNoValue={() =>
          previewContent === "" ? (
            <NoFile message="Invalid format, failed to parse file" />
          ) : (
            <NoFile message="No template files found" />
          )
        }
        language="handlebars"
      />
    </Stack>
  );
};
