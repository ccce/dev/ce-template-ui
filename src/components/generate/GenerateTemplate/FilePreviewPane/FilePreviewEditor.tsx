import { Editor, type Monaco } from "@monaco-editor/react";
import { styled } from "@mui/material";
import { monokaiTheme } from "./monokaiTheme";

const EditorWrapper = styled("div")(() => ({
  height: "100%",
  "&  .monaco-editor-overlaymessage": {
    display: "none !important"
  },
  "& .monaco-editor, & .overflow-guard, & .monaco-scrollable-element, & .monaco-editor-background":
    {
      borderRadius: "4px !important",
      zIndex: 1
    }
}));

const setTheme = (monaco: Monaco) => {
  monaco.editor.defineTheme("monokai", monokaiTheme);
  monaco.editor.setTheme("monokai");
};

export const FilePreviewEditor = ({ value = "", language = "handlebars" }) => {
  return (
    <EditorWrapper>
      <Editor
        height="100%"
        defaultValue={value}
        language={language}
        value={value}
        loading={null}
        options={{
          minimap: {
            enabled: false
          },
          domReadOnly: true,
          readOnly: true,
          wordWrap: "on",
          fontSize: 13,
          overviewRulerLanes: 0,
          hideCursorInOverviewRuler: true,
          scrollBeyondLastLine: false,
          lineNumbersMinChars: 4,
          guides: {
            indentation: false
          },
          renderLineHighlight: "none",
          padding: { top: 10, bottom: 10 },
          scrollbar: {
            alwaysConsumeMouseWheel: false
          }
        }}
        beforeMount={setTheme}
        theme="monokai"
        keepCurrentModel
        saveViewState
      />
    </EditorWrapper>
  );
};
