import { ReactNode } from "react";
import {
  Alert,
  CircularProgress,
  Paper,
  Stack,
  styled,
  Box
} from "@mui/material";
import { FilePreviewEditor } from "./FilePreviewEditor";
import { ApiError } from "../../../../types/common";
import { getErrorMessage } from "../../../common/Alerts/ErrorHandling";

const Overlay = styled(Paper)({
  height: "100%",
  position: "absolute",
  top: 0,
  left: 0,
  zIndex: 2,
  width: "100%",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  backgroundColor: "rgba(0, 0, 0, 0.7)",
  textAlign: "center",
  padding: 16,
  borderRadius: 4
});

const ErrorAlert = styled(Box)({
  position: "absolute",
  bottom: "0",
  padding: "10px",
  width: "auto",
  maxWidth: "400px",
  zIndex: 9
});

interface FileContentPreviewProps {
  value?: string;
  renderOnNoValue?: () => ReactNode;
  isLoading?: boolean;
  error?: ApiError;
  language?: string;
}

export const FilePreview = ({
  value = undefined,
  renderOnNoValue,
  isLoading = false,
  error,
  language = "json"
}: FileContentPreviewProps) => {
  let overlay = null;

  if (value === undefined && renderOnNoValue) {
    overlay = <Overlay>{renderOnNoValue()}</Overlay>;
  }

  if (isLoading || value === "") {
    overlay = (
      <Overlay>
        <CircularProgress />
      </Overlay>
    );
  }

  if (error) {
    overlay = (
      <ErrorAlert>
        <Alert severity="error">{getErrorMessage(error)}</Alert>
      </ErrorAlert>
    );
  }

  return (
    <Stack
      width="100%"
      position="relative"
      height="500px"
      sx={{ backgroundColor: "#272822", borderRadius: "4px" }}
    >
      {overlay}
      <FilePreviewEditor
        value={value || ""}
        language={language}
      />
    </Stack>
  );
};
