import { TabPanel } from "@ess-ics/ce-ui-common";
import { LinearProgress, Stack } from "@mui/material";
import { useEffect, useMemo, useState } from "react";
import { skipToken } from "@reduxjs/toolkit/query";
import { ConfigFilePreview } from "./ConfigFilePreview";
import { TemplateFilePreview } from "./TemplateFilePreview";
import { useGenerateTemplateContext } from "../GenerateTemplateContext";
import { useGetTypeFileNamesQuery } from "../../../../store/templateApi";
import { ApiAlertError } from "../../../common/Alerts/ApiAlertError";

const ST_CMD_MUSTACHE = "st.cmd.mustache";

const templateTabProps = {
  variant: "scrollable",
  sx: {
    ".MuiTabScrollButton-root.Mui-disabled": { opacity: 1 },
    "& > div:first-of-type": {
      width: "auto",
      paddingRight: "10px",
      paddingLeft: "10px",
      marginRight: "5px"
    },
    "& > div:last-of-type": {
      width: "auto",
      paddingRight: "10px",
      paddingLeft: "10px",
      marginLeft: "5px"
    }
  }
};

export const FilePreviewPane = () => {
  const {
    type,
    selectedState: { configPreview, templateReference },
    setIgnoredFiles
  } = useGenerateTemplateContext();
  const { id: typeId } = type || {};

  const [selectedTemplateFile, setSelectedTemplateFile] = useState("");
  const [orderedTemplateFiles, setOrderedTemplateFiles] = useState<
    string[] | null
  >(null);

  const {
    data: typeFiles,
    isLoading: typeFilesLoading,
    error: typeFilesError
  } = useGetTypeFileNamesQuery(
    typeId
      ? {
          typeId,
          typeRevision: templateReference
        }
      : skipToken
  );
  const { type_files, ignored_files } = typeFiles || {};

  useEffect(() => {
    if (type_files) {
      const orderedTemplateFiles = [...type_files].sort((a) =>
        a === ST_CMD_MUSTACHE ? -1 : 1
      );
      setOrderedTemplateFiles(orderedTemplateFiles);
      setSelectedTemplateFile(orderedTemplateFiles[0]);
    }
    if (ignored_files) {
      setIgnoredFiles(ignored_files);
    }
  }, [typeFiles, ignored_files, setIgnoredFiles, type_files]);

  const templateTabs = useMemo(() => {
    return orderedTemplateFiles?.map((fileName) => ({
      label: fileName,
      content: (
        <TemplateFilePreview selectedTemplateFile={selectedTemplateFile} />
      )
    }));
  }, [orderedTemplateFiles, selectedTemplateFile]);

  const configTabs = useMemo(() => {
    return [
      {
        label: configPreview?.file_name,
        content: <ConfigFilePreview />
      }
    ];
  }, [configPreview]);

  return (
    <>
      {typeFilesLoading && <LinearProgress sx={{ marginTop: "10px" }} />}
      {typeFilesError && <ApiAlertError error={typeFilesError} />}
      {typeFiles && (
        <Stack
          sx={{ "& > div": { width: "50%" } }}
          flexDirection="row"
          alignItems="end"
          gap={1}
        >
          <TabPanel
            tabs={templateTabs}
            onTabChange={(_: number, label: string) =>
              setSelectedTemplateFile(label)
            }
            TabsProps={templateTabProps}
          />
          <TabPanel
            TabsProps={{
              sx: {
                marginLeft: "15px",
                "&.MuiTabs-root": {
                  display: !configPreview && "none"
                },
                ".MuiButtonBase-root": {
                  pointerEvents: "none"
                }
              }
            }}
            tabs={configTabs}
          />
        </Stack>
      )}
    </>
  );
};
