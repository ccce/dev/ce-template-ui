import { useEffect, useState } from "react";
import { FilePreview } from "./FilePreview";
import { useLazyFetchFileContentQuery } from "../../../../store/templateApi";
import { useGenerateTemplateContext } from "../GenerateTemplateContext";
import { NoFile } from "../../../common/NoFile";

export const ConfigFilePreview = () => {
  const {
    type,
    selectedState: { configPreview, configReference }
  } = useGenerateTemplateContext();

  const { configuration_project_id } = type || {};

  const [
    getConfigFileContent,
    {
      data: configFileContent,
      isLoading: configFileContentLoading,
      error: configFileContentError
    }
  ] = useLazyFetchFileContentQuery();
  const content = configFileContent?.content || "";

  const [invalidJsonError, setInvalidJsonError] = useState<
    { status: number; data: { description: string } } | undefined
  >(undefined);

  useEffect(() => {
    if (
      configuration_project_id &&
      configReference &&
      configPreview?.file_name
    ) {
      getConfigFileContent({
        projectId: configuration_project_id,
        projectReference: configReference,
        fileName: configPreview.file_name
      });
    }
  }, [
    configReference,
    configPreview,
    configuration_project_id,
    getConfigFileContent
  ]);

  useEffect(() => {
    if (content) {
      try {
        JSON.parse(content);
      } catch {
        setInvalidJsonError({
          status: 412,
          data: { description: "File cannot be parsed as it is not valid JSON" }
        });
      }
    }
  }, [content]);

  return (
    <FilePreview
      value={!configPreview ? undefined : content}
      isLoading={configFileContentLoading}
      error={configFileContentError || invalidJsonError}
      renderOnNoValue={() => (
        <NoFile message="No revision selected for preview" />
      )}
      language="json"
    />
  );
};
