import { SyntheticEvent, useMemo, useState } from "react";
import { Stack, Box } from "@mui/material";
import { GitRefAutocomplete } from "@ess-ics/ce-ui-common";
import { skipToken } from "@reduxjs/toolkit/query";
import { useGenerateTemplateContext } from "../GenerateTemplateContext";
import {
  GitReference,
  useListTagsAndCommitIdsQuery
} from "../../../../store/templateApi";

export const RevisionsSelector = () => {
  const {
    type,
    selectedState: { templateReference, configReference },
    setSelectedState
  } = useGenerateTemplateContext();

  const { type_project_id, configuration_project_id } = type || {};

  const {
    data: templateRevisions,
    isLoading: templateRevisionsIsLoading,
    isError: templateRevisionsIsError
  } = useListTagsAndCommitIdsQuery(
    type_project_id
      ? {
          projectId: type_project_id,
          reference: ""
        }
      : skipToken
  );

  const {
    data: configRevisions,
    isLoading: configRevisionsIsLoading,
    isError: configRevisionsIsError
  } = useListTagsAndCommitIdsQuery(
    configuration_project_id
      ? {
          projectId: configuration_project_id,
          reference: ""
        }
      : skipToken
  );

  const templateValue = useMemo(
    () =>
      templateRevisions?.find(
        (template) => template.reference === templateReference
      ),
    [templateReference, templateRevisions]
  );
  const configValue = useMemo(
    () =>
      configRevisions?.find((config) => config.reference === configReference),
    [configReference, configRevisions]
  );

  const [configInputValue, setConfigInputValue] = useState<string>("");

  return (
    <Stack
      gap={1}
      width="100%"
    >
      <GitRefAutocomplete
        id="templateRevisionSelect"
        value={templateValue || null}
        label="Template revision"
        options={templateRevisions}
        onGitQueryValueSelect={(_: SyntheticEvent, value: GitReference) => {
          setConfigInputValue("");
          setSelectedState((prevState) => ({
            ...prevState,
            configReference: "",
            templateReference: value?.reference || ""
          }));
        }}
        loading={templateRevisionsIsLoading}
        textFieldProps={{
          error: templateRevisionsIsError,
          helperText: templateRevisionsIsError
            ? "Unable to load template revisions"
            : null
        }}
      />

      <Stack
        flexDirection="row"
        alignItems="center"
        flexWrap="wrap"
        gap={2}
      >
        <Box sx={{ flex: 1 }}>
          <GitRefAutocomplete
            id="configRevisionSelect"
            value={configValue || null}
            inputValue={configInputValue}
            label="Config revision"
            options={configRevisions}
            disabled={!templateReference}
            onGitQueryStringChange={(_: SyntheticEvent, value: string) => {
              setConfigInputValue(value);
            }}
            onGitQueryValueSelect={(_: SyntheticEvent, value: GitReference) => {
              if (!value) {
                setConfigInputValue("");
              }
              setSelectedState((prevState) => ({
                ...prevState,
                configReference: value?.reference || ""
              }));
            }}
            loading={configRevisionsIsLoading}
            textFieldProps={{
              error: configRevisionsIsError,
              helperText: configRevisionsIsError
                ? "Unable to load config revisions"
                : null
            }}
          />
        </Box>
      </Stack>
    </Stack>
  );
};
