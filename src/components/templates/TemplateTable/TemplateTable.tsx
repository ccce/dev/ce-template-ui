/**
 * TemplateTable
 * Search for and browse iocs types
 */
import { useEffect, useCallback } from "react";
import {
  Table,
  SearchBar,
  usePagination,
  InternalLink,
  EllipsisText,
  EmptyValue
} from "@ess-ics/ce-ui-common";
import { useSearchParams } from "react-router-dom";
import { TypeBase, useListTypesQuery } from "../../../store/templateApi";
import { ROWS_PER_PAGE } from "../../../constants";

const columns = [
  { field: "name", headerName: "IOC type", flex: 2 },
  { field: "description", headerName: "Description", flex: 3 },
  {
    field: "total_iocs",
    headerName: "IOCs",
    align: "right",
    headerAlign: "right",
    flex: 1
  },
  {
    field: "latest_tag",
    headerName: "Latest revision",
    align: "right",
    headerAlign: "right",
    flex: 1
  }
];

function createRow(template: TypeBase) {
  return {
    id: template.id,
    name: (
      <InternalLink
        to={`/types/${template.id}`}
        label={`IOC Type Details, ${template.name}`}
      >
        {template.name}
      </InternalLink>
    ),
    description: template.description ? (
      <EllipsisText>{template.description}</EllipsisText>
    ) : (
      <EmptyValue />
    ),
    total_iocs: template.number_of_iocs,
    latest_tag: (
      <InternalLink
        to={`/jobs/${template.latest_operation_id}`}
        label={`Job Details, ${template.name}, revision ${template.latest_ioc_tag}`}
      >
        {template.latest_ioc_tag}
      </InternalLink>
    )
  };
}

export function TemplateTable() {
  const [searchParams, setSearchParams] = useSearchParams({ query: "" });

  const setSearch = useCallback(
    (query: string) => {
      setSearchParams({ query }, { replace: true });
    },
    [setSearchParams]
  );

  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0]
  });

  const { data: listTypes, isLoading } = useListTypesQuery({
    query: searchParams.get("query"),
    ...pagination
  });

  useEffect(
    () => setPagination({ totalCount: listTypes?.total_count ?? 0 }),
    [setPagination, listTypes?.total_count]
  );

  const params = {
    columns,
    rows: listTypes?.types?.map((template) => createRow(template)) || [],
    loading: isLoading,
    pagination,
    onPage: setPagination
  };

  return (
    <SearchBar
      search={setSearch}
      query={searchParams.get("query")}
      loading={isLoading}
    >
      <div style={{ width: "100%" }}>
        <Table {...params} />
      </div>
    </SearchBar>
  );
}
