import { Box, Stack, TextField, Typography } from "@mui/material";
import { useCallback, useState } from "react";

const TEMPLATE_NAME_REGEX = "^(?=.{1,20}$)[a-z0-9]+(_[a-z0-9]+)*$";

interface TemplateNameInputProps {
  templateName: string;
  setTemplateName: React.Dispatch<React.SetStateAction<string>>;
  setTemplateDescription: React.Dispatch<React.SetStateAction<string>>;
}

export const TemplateNameInput = ({
  templateName,
  setTemplateName,
  setTemplateDescription
}: TemplateNameInputProps) => {
  const [templateNameInvalid, setTemplateNameInvalid] = useState(false);

  const handleTemplateValidation = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const reg = new RegExp(TEMPLATE_NAME_REGEX);
      const invalid = e.target.value.length !== 0 && !reg.test(e.target.value);
      setTemplateNameInvalid(invalid);
      setTemplateName(invalid ? "" : e.target.value);
    },
    [setTemplateName]
  );

  return (
    <Stack
      width="100%"
      gap={1}
    >
      <Box>
        <TextField
          autoComplete="off"
          id="iocTypeName"
          label="IOC type name"
          variant="outlined"
          fullWidth
          onChange={handleTemplateValidation}
          error={templateNameInvalid}
          helperText={
            templateNameInvalid
              ? "Only lowercase alphanumeric chars and underscores are allowed in ioc type name (max 20 chars)"
              : null
          }
          inputProps={{
            "aria-describedby": "iocTypeName-name-preview"
          }}
          sx={{
            "& .MuiFormHelperText-root": {
              maxWidth: "450px"
            }
          }}
        />
        <Typography
          variant="body2"
          fontStyle="italic"
          id="iocTypeName-name-preview"
          marginY="10px"
        >
          The IOC type name will be the root of the IOC instance names,
          following the pattern:
          <br />
          <Box
            sx={{ fontFamily: "Monospace" }}
            component="span"
          >
            e3-ioc-
            <Box
              sx={{ fontWeight: "bold", display: "inline" }}
              component="span"
            >
              {templateName ? templateName : "{IOC type name}"}
            </Box>
            -&#123;
            <Box
              sx={{ fontWeight: "bold", display: "inline" }}
              component="span"
            >
              instance name
            </Box>
            &#125;
          </Box>
        </Typography>
      </Box>
      <TextField
        autoComplete="off"
        id="iocTypeDescription"
        label="IOC type description"
        variant="outlined"
        multiline
        minRows={3}
        fullWidth
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setTemplateDescription(e.target.value)
        }
      />
    </Stack>
  );
};
