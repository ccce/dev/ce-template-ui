import { Box, Typography, useMediaQuery } from "@mui/material";
import { useSearchParams } from "react-router-dom";
import {
  CreateFromBlankCardContent,
  CreateFromExampleCardContent
} from "./Cards";
import { theme } from "../../../style/Theme";
import { ActionCard } from "../../common/ActionCard/ActionCard";

export const CreateTemplate = () => {
  const [params, setParams] = useSearchParams();
  const isDesktop = useMediaQuery(theme.breakpoints.up("sm"));

  return (
    <Box
      sx={{ height: "100%", display: "flex", flexDirection: "column", gap: 2 }}
    >
      <Typography
        variant="h2"
        textAlign="center"
      >
        Create a new IOC Type
      </Typography>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-around",
          flexDirection: isDesktop ? "row" : "column",
          gap: 2,
          height: "100%"
        }}
      >
        <ActionCard onClick={() => setParams({ ...params, view: "blank" })}>
          <CreateFromBlankCardContent />
        </ActionCard>
        <ActionCard onClick={() => setParams({ ...params, view: "example" })}>
          <CreateFromExampleCardContent />
        </ActionCard>
      </Box>
    </Box>
  );
};
