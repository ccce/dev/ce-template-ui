/**
 * CreateTemplateFromExample
 * Creating a new template from example repos
 */
import { useCallback, useEffect, useState } from "react";
import { Box, Button, Grid, LinearProgress } from "@mui/material";
import { useNavigate, useSearchParams } from "react-router-dom";
import { CreateFromExampleCardContent } from "./Cards";
import { TemplateNameInput } from "./TemplateNameInput";
import { ExampleProjectInput } from "./ExampleProjectInput";
import { PATHS } from "../../navigation/Routes";
import {
  GitProject,
  useCreateTypeAndConfigurationMutation
} from "../../../store/templateApi";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";
import { RootPaper } from "../../common/container/RootPaper";
import { useCustomSnackbar } from "../../common/Snackbar";

export function CreateFromExample() {
  const [templateName, setTemplateName] = useState("");
  const [templateDescription, setTemplateDescription] = useState("");
  const [exampleProject, setExampleProject] = useState<GitProject | undefined>(
    undefined
  );
  const [disableButton, setDisableButton] = useState(false);
  const { showSuccess } = useCustomSnackbar();
  const [, setSearchParams] = useSearchParams();
  const navigate = useNavigate();

  const [callCreateAPI, { error, isLoading, data: template }] =
    useCreateTypeAndConfigurationMutation();

  useEffect(() => {
    setDisableButton(false);
  }, [isLoading]);

  useEffect(() => {
    if (template) {
      showSuccess(`IOC type ${template?.name} created`);
      navigate(`${PATHS.PATH_TYPES}/${template?.type_project_id}`);
    }
  }, [template, showSuccess, navigate]);

  const createTemplate = useCallback(() => {
    callCreateAPI({
      exampleProjectId: exampleProject?.project_id,
      createTypeRequest: {
        type_name: templateName,
        description: templateDescription
      }
    });
  }, [callCreateAPI, templateName, templateDescription, exampleProject]);

  const onConfirm = useCallback(() => {
    setDisableButton(true);
    createTemplate();
  }, [createTemplate]);

  return (
    <RootPaper
      sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          gap: 2,
          maxWidth: "800px",
          width: "100%"
        }}
      >
        <Box>
          <CreateFromExampleCardContent titleProps={{ component: "h2" }} />
        </Box>
        <ExampleProjectInput
          exampleProject={exampleProject}
          setExampleProject={setExampleProject}
        />
        <TemplateNameInput
          templateName={templateName}
          setTemplateName={setTemplateName}
          setTemplateDescription={setTemplateDescription}
        />
        <Grid
          item
          container
          paddingY={1}
        >
          {isLoading && <LinearProgress sx={{ width: "100%" }} />}
          {error && (
            <Box sx={{ width: "100%" }}>
              <ApiAlertError error={error} />
            </Box>
          )}
        </Grid>
        <Grid
          item
          container
        >
          <Button
            onClick={() => setSearchParams({})}
            disabled={disableButton}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            disabled={!(templateName && exampleProject) || disableButton}
            onClick={onConfirm}
          >
            Create
          </Button>
        </Grid>
      </Box>
    </RootPaper>
  );
}
