/**
 * CreateTemplateFromBlank
 * Creating a new template from blank files (and without IOCs)
 */
import { useState, useCallback, useEffect } from "react";
import { Button, LinearProgress, Grid, Box } from "@mui/material";
import { useNavigate, useSearchParams } from "react-router-dom";
import { CreateFromBlankCardContent } from "./Cards";
import { TemplateNameInput } from "./TemplateNameInput";
import { PATHS } from "../../navigation/Routes";
import { useCreateTypeAndConfigurationMutation } from "../../../store/templateApi";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";
import { useCustomSnackbar } from "../../common/Snackbar";
import { RootPaper } from "../../common/container/RootPaper";

export function CreateFromBlank() {
  const [templateName, setTemplateName] = useState("");
  const [templateDescription, setTemplateDescription] = useState("");
  const [disableButton, setDisableButton] = useState(false);
  const { showSuccess } = useCustomSnackbar();
  const navigate = useNavigate();
  const [, setSearchParams] = useSearchParams();

  const [createTypeAndConfiguration, { error, isLoading, data: template }] =
    useCreateTypeAndConfigurationMutation();

  useEffect(() => {
    if (isLoading) {
      setDisableButton(false);
    }
  }, [isLoading]);

  useEffect(() => {
    if (template) {
      showSuccess(`IOC type ${template?.name} created`);
      navigate(`${PATHS.PATH_TYPES}/${template?.type_project_id}`);
    }
  }, [template, showSuccess, navigate]);

  const onConfirm = useCallback(() => {
    setDisableButton(true);
    createTypeAndConfiguration({
      createTypeRequest: {
        type_name: templateName,
        description: templateDescription
      }
    });
  }, [createTypeAndConfiguration, templateName, templateDescription]);

  return (
    <RootPaper
      sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start",
          gap: 2,
          maxWidth: "600px"
        }}
      >
        <Box>
          <CreateFromBlankCardContent titleProps={{ component: "h2" }} />
        </Box>
        <TemplateNameInput
          templateName={templateName}
          setTemplateName={setTemplateName}
          setTemplateDescription={setTemplateDescription}
        />
        <Grid
          item
          container
          paddingY={1}
        >
          {isLoading && <LinearProgress sx={{ width: "100%" }} />}
          {error && (
            <Box sx={{ width: "100%" }}>
              <ApiAlertError error={error} />
            </Box>
          )}
        </Grid>
        <Grid
          item
          container
        >
          <Button
            onClick={() => setSearchParams({})}
            disabled={disableButton}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            disabled={!templateName || disableButton}
            onClick={onConfirm}
          >
            Create
          </Button>
        </Grid>
      </Box>
    </RootPaper>
  );
}
