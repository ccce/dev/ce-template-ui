import AddIcon from "@mui/icons-material/Add";
import PostAddIcon from "@mui/icons-material/PostAdd";
import { ActionCardContent } from "../../common/ActionCard/ActionCard";

export const CreateFromBlankCardContent = (props: Record<string, unknown>) => {
  return (
    <ActionCardContent
      title="Create from blank"
      subtitle="Create a blank IOC type that can be used to generate IOCs"
      SvgIcon={AddIcon}
      {...props}
    />
  );
};

export const CreateFromExampleCardContent = (
  props: Record<string, unknown>
) => {
  return (
    <ActionCardContent
      title="Create from example"
      subtitle="Create an IOC type from one of several examples"
      SvgIcon={PostAddIcon}
      {...props}
    />
  );
};
