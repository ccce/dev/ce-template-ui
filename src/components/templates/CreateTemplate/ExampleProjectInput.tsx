import { ExternalButtonLink } from "@ess-ics/ce-ui-common";
import {
  Box,
  Button,
  Grid,
  InputAdornment,
  Skeleton,
  TextField,
  Typography
} from "@mui/material";
import {
  GitProject,
  useListExampleProjectsQuery
} from "../../../store/templateApi";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";

interface ExampleProjectInputProps {
  exampleProject: GitProject | undefined;
  setExampleProject: React.Dispatch<
    React.SetStateAction<GitProject | undefined>
  >;
}

export const ExampleProjectInput = ({
  exampleProject,
  setExampleProject
}: ExampleProjectInputProps) => {
  const { error, data: examples, isLoading } = useListExampleProjectsQuery();

  return (
    <Box width="100%">
      <Typography mb={1}>Example Project</Typography>

      {isLoading && (
        <Skeleton
          variant="rounded"
          height={100}
          animation="wave"
          width={"100%"}
          sx={{ bgcolor: "grey.100" }}
        />
      )}

      {error && <ApiAlertError error={error} />}

      {!exampleProject &&
        examples?.map((example) => (
          <Grid
            display="flex"
            flexDirection="row"
            justifyContent="space-between"
            alignItems="center"
            gap={1}
            py={1}
            px={2}
            key={example.project_id}
          >
            <Box>
              <Typography
                mb={0.5}
                fontWeight={600}
              >
                {example.project_name}
              </Typography>
              <Typography variant="body2">{example.description}</Typography>
            </Box>
            <Box>
              <Button
                variant="contained"
                onClick={() => setExampleProject(example)}
                sx={{ marginRight: "8px" }}
              >
                Select This Example
              </Button>
              <ExternalButtonLink
                href={example.project_url}
                variant="outlined"
                aria-label={`Preview example project ${example.project_name}. Opens in new tab`}
              >
                Preview
              </ExternalButtonLink>
            </Box>
          </Grid>
        ))}

      {exampleProject && examples && (
        <Box pt={2}>
          <TextField
            label="Selected Project"
            value={exampleProject?.project_name}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Button
                    variant="contained"
                    onClick={() => setExampleProject(undefined)}
                  >
                    Change Project
                  </Button>
                </InputAdornment>
              )
            }}
            fullWidth
            disabled
          />
        </Box>
      )}
    </Box>
  );
};
