/**
 * TemplateDetails
 */
import { Grid, Badge, Tooltip, Box, Typography, Stack } from "@mui/material";
import {
  KeyValueTable,
  ExternalLink,
  EmptyValue,
  IconBadge,
  AlertBanner
} from "@ess-ics/ce-ui-common";
import WidgetsIcon from "@mui/icons-material/Widgets";
import { IocTable } from "../IocTable";
import { useGetTypeQuery } from "../../../store/enhancedApi";
import { Type } from "../../../store/templateApi";

interface TemplateStatusProps {
  typeId: number;
}

const getTemplateDetails = (template: Type) => ({
  Description: template.description ?? <EmptyValue />, // Capitalized Description as becomes display text.
  "template repository": (
    <ExternalLink
      href={template.type_project_url}
      label="Template repository"
    >
      {template.type_project_url}
    </ExternalLink>
  ),
  "configuration repository": (
    <ExternalLink
      href={template.configuration_project_url}
      label="Configuration repository"
    >
      {template.configuration_project_url}
    </ExternalLink>
  )
});

export const TemplateStatus = ({ typeId }: TemplateStatusProps) => {
  const { data: template } = useGetTypeQuery({ typeId: Number(typeId) });

  return (
    <>
      {template && (
        <Grid
          container
          spacing={2}
          width="100%"
        >
          <Grid
            item
            xs={12}
            md={12}
          >
            {template.deleted && (
              <AlertBanner
                type={"info"}
                message={
                  "This is an archived IOC type. Resources are read-only."
                }
              />
            )}
          </Grid>
          <Grid
            item
            xs={12}
            md={12}
          >
            <IconBadge
              icon={
                <Tooltip
                  title={`Number of IOCs: ${template.number_of_iocs}`}
                  placement="right"
                >
                  <Box>
                    <WidgetsIcon sx={{ fontSize: 20 }} />
                    <Badge
                      badgeContent={template.number_of_iocs}
                      color="primary"
                      sx={{ top: "-1rem", left: "0.5rem" }}
                    />
                  </Box>
                </Tooltip>
              }
              title={template.name}
            />

            <KeyValueTable
              obj={getTemplateDetails(template)}
              variant="overline"
            />
          </Grid>
          <Grid
            item
            xs={12}
            md={12}
          >
            {template?.id && (
              <Stack gap={2}>
                <Typography
                  variant="h3"
                  component="h2"
                >
                  IOCs
                </Typography>
                <IocTable templateId={template.id} />
              </Stack>
            )}
          </Grid>
        </Grid>
      )}
    </>
  );
};
