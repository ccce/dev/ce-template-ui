import { DEFAULT_POLLING_INTERVAL_MILLIS } from "../../../store/apiConfig";
import { Ioc, useGetProjectInformationQuery } from "../../../store/templateApi";

interface GitInstanceNameProps {
  ioc: Ioc;
}

const IOC_BASE_NAME = "e3-ioc";

const getParsedName = (projectName: string = "", typeName: string = "") =>
  projectName.replace(`${IOC_BASE_NAME}-`, "").replace(`${typeName}-`, "");

export const GitInstanceName = ({ ioc }: GitInstanceNameProps) => {
  const { git_project_id, configuration_name, type_name } = ioc;

  const { data: gitProjectInfo } = useGetProjectInformationQuery(
    { projectId: git_project_id ?? 0 },
    { pollingInterval: DEFAULT_POLLING_INTERVAL_MILLIS }
  );

  return (
    <>
      {gitProjectInfo
        ? getParsedName(gitProjectInfo.project_name, type_name)
        : configuration_name}
    </>
  );
};
