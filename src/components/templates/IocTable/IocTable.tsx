/**
 * IocTable
 * Detailed view with IOC data table
 */
import { useEffect, useCallback } from "react";
import {
  Table,
  SearchBar,
  usePagination,
  EmptyValue
} from "@ess-ics/ce-ui-common";
import { useSearchParams } from "react-router-dom";
import { GitInstanceName } from "./GitInstanceName";
import { IocGitInfo } from "../../common/IocGitInfo";
import { Ioc } from "../../../store/templateApi";
import { useListIocsQuery } from "../../../store/enhancedApi";
import { ROWS_PER_PAGE } from "../../../constants";

interface IocTableProps {
  templateId: number;
}

const columns = [
  { field: "config_name", headerName: "Instance name" },
  {
    field: "git_revision",
    headerName: "Revision",
    align: "right",
    headerAlign: "right"
  }
];

const createRow = (ioc: Ioc) => {
  const { id, git_tag, git_project_id } = ioc;

  return {
    config_name: <GitInstanceName ioc={ioc} />,
    id: id,
    git_revision:
      git_tag && git_project_id ? (
        <IocGitInfo
          gitTag={git_tag}
          gitProjectId={git_project_id}
        />
      ) : (
        <EmptyValue />
      )
  };
};

export const IocTable = ({ templateId }: IocTableProps) => {
  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0]
  });

  const [searchParams, setSearchParams] = useSearchParams({ query: "" });

  const { data: iocList, isLoading } = useListIocsQuery({
    query: searchParams.get("query"),
    request_type: "LATEST",
    ...pagination,
    typeId: templateId
  });

  const setSearch = useCallback(
    (query: string) => {
      setSearchParams({ query }, { replace: true });
    },
    [setSearchParams]
  );

  useEffect(
    () => setPagination({ totalCount: iocList?.total_count ?? 0 }),
    [setPagination, iocList?.total_count]
  );

  const params = {
    columns,
    rows: iocList?.iocs?.map((ioc: Ioc) => createRow(ioc)) || [],
    loading: isLoading,
    rowHeight: 60,
    pagination,
    onPage: setPagination
  };

  return (
    <SearchBar
      search={setSearch}
      query={searchParams.get("query")}
      loading={isLoading}
    >
      <Table {...params} />
    </SearchBar>
  );
};
