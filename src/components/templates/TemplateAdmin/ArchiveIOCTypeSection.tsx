import { useState, useCallback, useEffect } from "react";
import { SimpleAccordion, ConfirmationDialog } from "@ess-ics/ce-ui-common";
import { Box, Button, LinearProgress } from "@mui/material";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";
import { Type } from "../../../store/templateApi";
import {
  useArchiveTypeMutation,
  useUnarchiveTypeMutation
} from "../../../store/enhancedApi";

interface ArchiveTemplateAdminProps {
  template: Type;
  disableButton: boolean;
  onDisableButton: (isDisabled: boolean) => void;
}

const ARCHIVE = "Archive";
const UN_ARCHIVE = "Unarchive";

export const ArchiveIOCTypeSection = ({
  template,
  disableButton,
  onDisableButton
}: ArchiveTemplateAdminProps) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [accordionExpanded, setAcccordionExpanded] = useState(false);
  const currentAction = template.deleted ? UN_ARCHIVE : ARCHIVE;
  const text = `${currentAction} IOC type`;

  const [
    callArchiveIOCType,
    { isLoading: isArchivingLoading, error: archivingError }
  ] = useArchiveTypeMutation();

  const [
    callUnarchiveIOCType,
    { isLoading: isUnarchivingLoading, error: unArchivingError }
  ] = useUnarchiveTypeMutation();

  const onConfirm = useCallback(async () => {
    if (template.id) {
      onDisableButton(true);
      if (currentAction === ARCHIVE) {
        await callArchiveIOCType({ typeId: template.id });
      } else {
        await callUnarchiveIOCType({ typeId: template.id });
      }
      setModalOpen(false);
      onDisableButton(false);
    }
  }, [
    template,
    callArchiveIOCType,
    callUnarchiveIOCType,
    currentAction,
    onDisableButton
  ]);

  useEffect(() => {
    if (unArchivingError || archivingError) {
      setModalOpen(false);
    }
  }, [unArchivingError, archivingError]);

  return (
    <SimpleAccordion
      summary={text}
      expanded={accordionExpanded}
      onChange={() => setAcccordionExpanded((prev) => !prev)}
    >
      <Box sx={{ marginBottom: 2 }}>
        {(isArchivingLoading || isUnarchivingLoading) && <LinearProgress />}
        {(archivingError || unArchivingError) && (
          <ApiAlertError error={archivingError || unArchivingError} />
        )}
      </Box>

      <Button
        color="primary"
        variant="contained"
        onClick={() => setModalOpen(true)}
        disabled={isArchivingLoading || isUnarchivingLoading || disableButton}
      >
        {text}
      </Button>
      <ConfirmationDialog
        title={text}
        confirmText={currentAction}
        cancelText="Cancel"
        content={`Are you sure want to ${currentAction.toLowerCase()} ${
          template.name
        }?`}
        open={modalOpen}
        onClose={() => setModalOpen(false)}
        onCancel={() => setModalOpen(false)}
        onConfirm={onConfirm}
      />
    </SimpleAccordion>
  );
};
