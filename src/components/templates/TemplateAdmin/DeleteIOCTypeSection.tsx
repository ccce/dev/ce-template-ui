/**
 * DeleteTemplateAdmin
 * Admin page for deleting a type
 */
import { useState, useCallback } from "react";
import { Button, Grid, Alert, Typography, LinearProgress } from "@mui/material";
import {
  SimpleAccordion,
  ConfirmDangerActionDialog
} from "@ess-ics/ce-ui-common";
import { useNavigate } from "react-router-dom";
import { PATHS } from "../../navigation/Routes";
import { Type, useDeleteTypeMutation } from "../../../store/templateApi";
import { ApiError, ErrorMessage } from "../../../types/common";

interface DeleteTemplateAdminProps {
  template: Type;
  disableButton: boolean;
  onDisableButton: (isDisabled: boolean) => void;
}

const getDialogText = (template: Type) => {
  const hasIOCS =
    template.number_of_iocs !== undefined && template.number_of_iocs > 0;

  return (
    <>
      {hasIOCS ? (
        <Typography
          fontSize="small"
          fontWeight="bold"
          sx={{ marginBottom: "0.5rem" }}
        >
          Note that this IOC type has instances connected, which also would be
          deleted.
        </Typography>
      ) : null}
      <Typography>
        Type in the complete IOC type name to confirm that you really want to
        delete the IOC type. This action will delete all{" "}
        {hasIOCS ? "instances and " : ""}operations.
      </Typography>
    </>
  );
};

export function DeleteIOCTypeSection({
  template,
  disableButton,
  onDisableButton
}: DeleteTemplateAdminProps) {
  const [modalOpen, setModalOpen] = useState(false);
  const [accordionExpanded, setAcccordionExpanded] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const navigate = useNavigate();
  const [callDeleteAPI, { isLoading }] = useDeleteTypeMutation();

  const onConfirm = useCallback(async () => {
    onDisableButton(true);
    if (template.id) {
      callDeleteAPI({ typeId: template.id })
        .unwrap()
        .then(() => navigate(PATHS.PATH_BASE))
        .catch((rejected: ApiError) => {
          if (rejected && "data" in rejected) {
            const error = rejected as ErrorMessage;
            setErrorMessage(error?.data?.description ?? "Unknown error");
          } else {
            setErrorMessage("Unknown error");
          }
          setModalOpen(false);
          onDisableButton(false);
        });
    }
  }, [template, callDeleteAPI, navigate, onDisableButton]);

  return (
    <SimpleAccordion
      summary="Delete IOC type"
      expanded={accordionExpanded}
      onChange={() => setAcccordionExpanded((prev) => !prev)}
    >
      <Grid
        container
        spacing={2}
      >
        <Grid
          item
          xs={12}
        >
          {errorMessage && <Alert severity="error">{errorMessage}</Alert>}
          {isLoading && <LinearProgress />}
        </Grid>
        <Grid
          item
          xs={12}
        >
          <Button
            color="primary"
            variant="contained"
            onClick={() => setModalOpen(true)}
            disabled={isLoading || disableButton}
          >
            Delete IOC type
          </Button>
          <ConfirmDangerActionDialog
            title="Delete IOC type"
            text={getDialogText(template)}
            maxWidth="md"
            confirmText="Delete"
            valueToCheck={template.name}
            open={modalOpen}
            onClose={() => setModalOpen(false)}
            onConfirm={onConfirm}
          />
        </Grid>
      </Grid>
    </SimpleAccordion>
  );
}
