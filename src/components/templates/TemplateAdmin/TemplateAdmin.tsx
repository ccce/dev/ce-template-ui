import { useState } from "react";
import { LinearProgress } from "@mui/material";
import { ModifyIOCTypeSection } from "./ModifyIOCTypeSection";
import { ArchiveIOCTypeSection } from "./ArchiveIOCTypeSection";
import { DeleteIOCTypeSection } from "./DeleteIOCTypeSection";
import { DeleteIOCTypeInstancesSection } from "./DeleteIOCTypeIOCsSection/DeleteIOCTypeInstancesSection";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";
import { useGetTypeQuery } from "../../../store/enhancedApi";

interface TemplateAdminProps {
  typeId: number;
}

export const TemplateAdmin = ({ typeId }: TemplateAdminProps) => {
  const [disableButton, setDisableButton] = useState(false);
  const {
    data: template,
    isLoading,
    error
  } = useGetTypeQuery({ typeId: Number(typeId) });

  return (
    <>
      {isLoading && <LinearProgress />}
      {error && <ApiAlertError error={error} />}
      {template && (
        <>
          <ModifyIOCTypeSection
            template={template}
            disableButton={disableButton}
            onDisableButton={(value: boolean) => setDisableButton(value)}
          />
          <DeleteIOCTypeInstancesSection
            template={template}
            disableButton={disableButton}
            onDisableButton={(value: boolean) => setDisableButton(value)}
          />
          <ArchiveIOCTypeSection
            template={template}
            disableButton={disableButton}
            onDisableButton={(value: boolean) => setDisableButton(value)}
          />
          <DeleteIOCTypeSection
            template={template}
            disableButton={disableButton}
            onDisableButton={(value: boolean) => setDisableButton(value)}
          />
        </>
      )}
    </>
  );
};
