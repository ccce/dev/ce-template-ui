import { useState, useCallback, useEffect } from "react";
import { TextField, Grid, Button, LinearProgress, Stack } from "@mui/material";
import { ConfirmationDialog, SimpleAccordion } from "@ess-ics/ce-ui-common";
import { Type } from "../../../store/templateApi";
import { useUpdateTypeMutation } from "../../../store/enhancedApi";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";

interface ModifyIOCTypeSectionProps {
  template: Type;
  disableButton: boolean;
  onDisableButton: (isDisabled: boolean) => void;
}

export const ModifyIOCTypeSection = ({
  template,
  disableButton,
  onDisableButton
}: ModifyIOCTypeSectionProps) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [accordionExpanded, setAcccordionExpanded] = useState(false);
  const [newName, setNewName] = useState(template.name ?? "");
  const [newDescription, setNewDescription] = useState(
    template.description ?? ""
  );
  const [callUpdateType, { isLoading, error }] = useUpdateTypeMutation();

  const onConfirm = useCallback(async () => {
    onDisableButton(true);
    await callUpdateType({
      typeId: template.id,
      updateTypeRequest: {
        type_new_name: newName,
        description: newDescription
      }
    });
    setModalOpen(false);
    onDisableButton(false);
  }, [template, newName, newDescription, callUpdateType, onDisableButton]);

  useEffect(() => {
    if (error) {
      setModalOpen(false);
    }
  }, [error]);

  return (
    <SimpleAccordion
      summary="Modify IOC type"
      expanded={accordionExpanded}
      onChange={() => setAcccordionExpanded((prev) => !prev)}
    >
      <Grid
        container
        spacing={2}
      >
        <Grid
          item
          container
          xs={12}
        >
          <Stack
            gap={1}
            width="100%"
          >
            <TextField
              value={newName}
              label="IOC type name"
              variant="outlined"
              fullWidth
              onChange={(event) => setNewName(event.target.value)}
              disabled={disableButton}
            />
            <TextField
              value={newDescription}
              label="IOC type description"
              variant="outlined"
              fullWidth
              multiline
              minRows={3}
              onChange={(event) => setNewDescription(event.target.value)}
              disabled={disableButton}
            />
          </Stack>
        </Grid>
        <Grid
          item
          xs={12}
        >
          {isLoading && <LinearProgress sx={{ width: "100%" }} />}
          {error && <ApiAlertError error={error} />}
        </Grid>
        <Grid
          item
          xs={12}
        >
          <Button
            color="primary"
            variant="contained"
            onClick={() => setModalOpen(true)}
            disabled={disableButton}
          >
            Modify IOC type
          </Button>
          <ConfirmationDialog
            title="Modify IOC type"
            confirmText="Modify"
            cancelText="Cancel"
            content={`Are you sure want to modify ${template.name}?`}
            open={modalOpen}
            onClose={() => setModalOpen(false)}
            onCancel={() => setModalOpen(false)}
            onConfirm={onConfirm}
          />
        </Grid>
      </Grid>
    </SimpleAccordion>
  );
};
