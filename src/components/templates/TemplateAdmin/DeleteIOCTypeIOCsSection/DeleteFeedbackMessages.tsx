import { Alert, Box } from "@mui/material";
import { NameAndGitRepoWithMessage } from "../../../../store/templateApi";

interface DeleteFeedbackMessagesProps {
  messages: NameAndGitRepoWithMessage[];
  onCloseAlert: (type: string) => void;
}

export const DeleteFeedbackMessages = ({
  messages,
  onCloseAlert
}: DeleteFeedbackMessagesProps) => {
  const errorMessages = messages.filter(
    (message) => message.result === "ERROR"
  );
  const successMessages = messages.filter(
    (message) => message.result === "SUCCESS"
  );

  return (
    <Box>
      {successMessages.length > 0 ? (
        <Alert
          severity="success"
          onClose={() => onCloseAlert("SUCCESS")}
          sx={{ marginBottom: "10px" }}
        >
          Success
          <ul>
            {successMessages.map((item) => (
              <li key={item.git_project_id}>
                {item.configuration_name}: {item.message}
              </li>
            ))}
          </ul>
        </Alert>
      ) : null}

      {errorMessages.length > 0 ? (
        <Alert
          severity="error"
          onClose={() => onCloseAlert("ERROR")}
          sx={{ marginBottom: "10px" }}
        >
          Error
          <ul>
            {errorMessages.map((item) => (
              <li key={item.git_project_id}>
                {item.configuration_name} {item.git_project_id} : {item.message}
              </li>
            ))}
          </ul>
        </Alert>
      ) : null}
    </Box>
  );
};
