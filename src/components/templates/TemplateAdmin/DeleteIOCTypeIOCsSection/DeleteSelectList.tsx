import { useState, useCallback, FormEvent } from "react";
import { Button, FormGroup, Box } from "@mui/material";
import { ConfirmDangerActionDialog } from "@ess-ics/ce-ui-common";
import { CheckboxList } from "../../../common/CheckboxList";

interface DeleteSelectListProps {
  listData: { id: string; label: string }[];
  disableButton: boolean;
  onDelete: (selected: string[]) => void;
}

export const DeleteSelectList = ({
  listData,
  disableButton,
  onDelete
}: DeleteSelectListProps) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [selected, setSelected] = useState<string[]>([]);

  const promptConfirmation = (e: FormEvent) => {
    e.preventDefault();
    setModalOpen(true);
  };

  const handleSubmit = useCallback(() => {
    setModalOpen(false);
    onDelete(selected);
    setSelected([]);
  }, [selected, onDelete]);

  return (
    <>
      <form onSubmit={(e) => promptConfirmation(e)}>
        <FormGroup sx={{ marginLeft: "0.4rem" }}>
          <CheckboxList
            listData={listData}
            selected={selected}
            onSelect={(selected) => setSelected(selected)}
          />
          <Box
            sx={{ width: "100%", display: "flex", justifyContent: "flex-end" }}
          >
            <Button
              type="submit"
              variant="contained"
              color="primary"
              disabled={selected.length === 0 || disableButton}
              aria-haspopup="dialog"
            >
              Delete
            </Button>
          </Box>
        </FormGroup>
      </form>

      {modalOpen ? (
        <ConfirmDangerActionDialog
          title="Delete IOC instances"
          text={`You are about to delete ${selected.length} ${
            selected.length === 1 ? "instance" : "instances"
          }. To confirm deletion, type in the correct number of instances to delete.`}
          maxWidth="md"
          confirmText="Delete"
          valueToCheck={String(selected.length)}
          open={modalOpen}
          onClose={() => setModalOpen(false)}
          onConfirm={handleSubmit}
        />
      ) : null}
    </>
  );
};
