import { useState, useEffect } from "react";
import { SimpleAccordion } from "@ess-ics/ce-ui-common";
import { Alert, Box, Typography, LinearProgress } from "@mui/material";
import { DeleteFeedbackMessages } from "./DeleteFeedbackMessages";
import { DeleteSelectList } from "./DeleteSelectList";
import {
  Type,
  NameAndGitRepo,
  NameAndGitRepoWithMessage,
  useLazyListIocsMinimalQuery
} from "../../../../store/templateApi";
import { useDeleteTypeIocsMutation } from "../../../../store/enhancedApi";
import { ApiAlertError } from "../../../common/Alerts/ApiAlertError";

interface DeleteIOCTypeInstancesSectionProps {
  template: Type;
  disableButton: boolean;
  onDisableButton: (isDisabled: boolean) => void;
}

const shapeIocs = (iocs: NameAndGitRepo[]): { id: string; label: string }[] =>
  iocs.map((ioc) => {
    return {
      id: String(ioc.git_project_id),
      label: ioc.configuration_name
    };
  });

export function DeleteIOCTypeInstancesSection({
  template,
  disableButton,
  onDisableButton
}: DeleteIOCTypeInstancesSectionProps) {
  const [listData, setListData] = useState<
    { id: string; label: string }[] | null
  >(null);
  const [actionFeedback, setActionFeedback] = useState<
    NameAndGitRepoWithMessage[] | []
  >([]);
  const [accordionExpanded, setAcccordionExpanded] = useState(false);

  const [
    getListIOCsMinimal,
    {
      data: listIocsMinimalData,
      isLoading: listIocsMinimalLoading,
      error: listIocsMinimalError
    }
  ] = useLazyListIocsMinimalQuery();

  const [
    deleteTypeIOCs,
    {
      data: deleteTypeIOCsData,
      isLoading: deleteTypeIOCsLoading,
      error: deleteTypeIOCsError
    }
  ] = useDeleteTypeIocsMutation();

  const handleCloseAlert = (typeOfMessage: string) => {
    setActionFeedback(
      actionFeedback?.filter(
        (message: NameAndGitRepoWithMessage) => message.result !== typeOfMessage
      )
    );
  };

  const handleDeleteIocs = async (selectedIocs: string[]) => {
    onDisableButton(true);
    const selected = listIocsMinimalData?.iocs?.filter((ioc: NameAndGitRepo) =>
      selectedIocs.includes(String(ioc.git_project_id))
    );

    await deleteTypeIOCs({
      typeId: template.id,
      deleteIocRequest: { iocs_to_delete: selected }
    });
    onDisableButton(false);
  };

  useEffect(() => {
    if (template) {
      getListIOCsMinimal({ typeId: template.id });
    }
  }, [template, getListIOCsMinimal]);

  useEffect(() => {
    if (listIocsMinimalData) {
      setListData(shapeIocs(listIocsMinimalData.iocs || []));
    }
  }, [listIocsMinimalData]);

  useEffect(() => {
    if (deleteTypeIOCsData) {
      setActionFeedback(deleteTypeIOCsData);
      getListIOCsMinimal({ typeId: template.id });
    }
  }, [deleteTypeIOCsData, getListIOCsMinimal, template.id]);

  return (
    <SimpleAccordion
      summary={
        <Typography
          component="h2"
          variant="h3"
        >
          Delete IOC instances
        </Typography>
      }
      expanded={accordionExpanded}
      onChange={() => setAcccordionExpanded((prev) => !prev)}
    >
      <Box
        sx={{
          height: 6,
          visibility:
            listIocsMinimalLoading || deleteTypeIOCsLoading
              ? "visible"
              : "hidden"
        }}
      >
        <LinearProgress sx={{ width: "100%" }} />
      </Box>

      {(listIocsMinimalError || deleteTypeIOCsError) && (
        <ApiAlertError error={listIocsMinimalError || deleteTypeIOCsError} />
      )}

      {actionFeedback && (
        <DeleteFeedbackMessages
          messages={actionFeedback}
          onCloseAlert={handleCloseAlert}
        />
      )}

      {listData && (
        <>
          {listData.length === 0 ? (
            <Alert severity="info">No IOCs found</Alert>
          ) : (
            <DeleteSelectList
              listData={listData}
              disableButton={disableButton}
              onDelete={handleDeleteIocs}
            />
          )}
        </>
      )}
    </SimpleAccordion>
  );
}
