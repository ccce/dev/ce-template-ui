/**
 * TemplateManagement
 */
import {
  Grid,
  Stack,
  Typography,
  Box,
  Button,
  LinearProgress
} from "@mui/material";
import { KeyValueTable, AlertBanner } from "@ess-ics/ce-ui-common";
import { useNavigate } from "react-router-dom";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";
import { PATHS } from "../../navigation/Routes";
import { JobTable } from "../../job/JobTable";
import { useGetTypeQuery } from "../../../store/enhancedApi";

interface TemplateManagementProps {
  typeId: number;
}

export const TemplateManagement = ({ typeId }: TemplateManagementProps) => {
  const navigate = useNavigate();
  const {
    data: template,
    isLoading,
    error
  } = useGetTypeQuery({ typeId: Number(typeId) });

  return (
    <>
      {isLoading && <LinearProgress />}
      {error && <ApiAlertError error={error} />}
      {template && (
        <Grid
          container
          spacing={2}
          width="100%"
        >
          <Grid
            item
            xs={12}
            md={12}
          >
            {template?.deleted && (
              <AlertBanner
                type={"info"}
                message={
                  "This is an archived IOC type. Resources are read-only."
                }
              />
            )}
          </Grid>
          <Grid
            item
            xs={12}
            md={12}
          >
            <Box
              display="flex"
              flexDirection="row-reverse"
              p={2}
              m={1}
            >
              <Button
                variant="contained"
                onClick={() =>
                  navigate(`${PATHS.PATH_GENERATE}?typeId=${template.id}`)
                }
                disabled={template.deleted}
              >
                Generate new revision(s)
              </Button>
            </Box>
          </Grid>
          <Grid
            item
            xs={12}
            md={12}
          >
            <KeyValueTable
              obj={{
                "registered by": template.created_by
              }}
              variant="overline"
            />
          </Grid>
          <Grid
            item
            xs={12}
            md={12}
          >
            <Stack gap={2}>
              <Typography
                variant="h3"
                component="h2"
              >
                Jobs
              </Typography>
              <JobTable templateId={template.id} />
            </Stack>
          </Grid>
        </Grid>
      )}
    </>
  );
};
