export const getStepLabel = (step: string) => {
  switch (step) {
    case "CREATE_GIT_PROJECT":
      return "Create git project";
    case "PROCESSING_TEMPLATE_AND_COMMIT":
      return "Generate revision";
    case "MOVE_INSTANCE":
      return "Move instance";
    case "ATTACH_INSTANCE":
      return "Attach instance";
    default:
      return step;
  }
};
