import CommitIcon from "@mui/icons-material/Commit";
import MoveUpIcon from "@mui/icons-material/MoveUp";
import FormatListBulletedAdd from "../../icons/format_list_bulleted_add.svg?react";

interface StatusMessage {
  message: string;
  alertType: string;
}

export const JOB_STATUS: Record<string, StatusMessage> = {
  QUEUED: {
    message: "Job is queued",
    alertType: "info"
  },
  RUNNING: {
    message: "Job is running",
    alertType: "info"
  },
  SUCCESSFUL: {
    message: "Job was successful",
    alertType: "success"
  },
  FAILED: {
    message: "Job failed",
    alertType: "error"
  },
  PARTIAL_FAILURE: {
    message: "Job had partial failures",
    alertType: "error"
  },
  UNKNOWN: {
    message: "Job status is unknown",
    alertType: "error"
  }
};

export enum JOB_TYPES {
  GENERATE_INSTANCE = "GENERATE_INSTANCE",
  ATTACH = "ATTACH",
  MOVE = "MOVE"
}

export const JOB_TYPE_ICONS = {
  GENERATE_INSTANCE: {
    label: "Generate revision",
    icon: CommitIcon
  },
  ATTACH: {
    label: "Attach",
    icon: FormatListBulletedAdd
  },
  MOVE: {
    label: "Move",
    icon: MoveUpIcon
  }
};
