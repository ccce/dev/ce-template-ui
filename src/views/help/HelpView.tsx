import { useEffect, useContext } from "react";
import { Help, GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { applicationSubTitle } from "../../components/common/Helper";
import { RootPaper } from "../../components/common/container/RootPaper";
import { env } from "../../config/env.js";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";

export function HelpView() {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);

  useEffect(() => {
    setTitle(`CE template ${applicationSubTitle()} / Help`);
  }, [setTitle]);

  return (
    <RootPaper>
      <Help
        summary='This is the templating part of the IOC toolchain in the controls ecosystem (CE); working name "CE template". It is a tool that manages IOC types and their related IOCs, using git to interact with repositories, and using mustache together with JSON to define templates and configurations.'
        docsHref="https://confluence.esss.lu.se/x/qu_dGQ"
        supportHref={env.SUPPORT_URL}
        version={env.TEMPLATE_FRONTEND_VERSION}
        apiHref="/api/swagger-ui/index.html"
      />
    </RootPaper>
  );
}
