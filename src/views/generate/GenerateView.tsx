/**
 * GenerateView
 * View for generating new type
 */
import { useGlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { LinearProgress } from "@mui/material";
import { GenerateTemplateProvider } from "../../components/generate/GenerateTemplate/GenerateTemplateProvider";
import { GenerateTemplate } from "../../components/generate/GenerateTemplate/GenerateTemplate";
import { GlobalAppBarContext } from "../../types/common";
import { ApiAlertError } from "../../components/common/Alerts/ApiAlertError";
import { applicationSubTitle } from "../../components/common/Helper";
import { useLazyGetTypeQuery } from "../../store/templateApi";

export function GenerateView() {
  const { setTitle }: GlobalAppBarContext = useGlobalAppBarContext();
  const [params] = useSearchParams();
  const typeId = Number(params.get("typeId"));

  const [getTemplate, { data: type, isLoading, error }] = useLazyGetTypeQuery();

  useEffect(() => {
    if (typeId) {
      getTemplate({ typeId });
    }
  }, [typeId, getTemplate]);

  useEffect(() => {
    setTitle(
      `CE template ${applicationSubTitle()} / Generate${
        type ? `: ${type?.name}` : ""
      }`
    );
  }, [type, setTitle]);

  return (
    <>
      {isLoading && (
        <LinearProgress sx={{ marginTop: "20px", width: "100%" }} />
      )}
      {error && <ApiAlertError error={error} />}
      {type && (
        <GenerateTemplateProvider>
          <GenerateTemplate type={type} />
        </GenerateTemplateProvider>
      )}
    </>
  );
}
