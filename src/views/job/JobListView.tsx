import { useEffect } from "react";
import { useGlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { GlobalAppBarContext } from "../../types/common";
import { applicationSubTitle } from "../../components/common/Helper";
import { RootPaper } from "../../components/common/container/RootPaper";
import { JobTable } from "../../components/job/JobTable";

export const JobListView = () => {
  const { setTitle }: GlobalAppBarContext = useGlobalAppBarContext();

  useEffect(() => {
    setTitle(`CE template ${applicationSubTitle()} / Job`);
  }, [setTitle]);

  return (
    <RootPaper>
      <JobTable />
    </RootPaper>
  );
};
