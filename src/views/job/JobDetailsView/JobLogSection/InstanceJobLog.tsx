import {
  EllipsisText,
  Table,
  STEPPER_STATES,
  SingleStateStepper
} from "@ess-ics/ce-ui-common";
import { GridRowClassNameParams } from "@mui/x-data-grid-pro";
import { Box } from "@mui/material";
import { getStepLabel } from "../../../../components/JobDetails/getStepLabel";
import { InstanceLogEntry } from "../../../../store/templateApi";

interface LogDetailsLogsRow {
  status: string;
  message: string;
  isFailed: boolean;
}

const getColumns = (logs: InstanceLogEntry[]) => {
  const hasErrorMessages = logs.some(
    (log) => log.status === STEPPER_STATES.FAILED
  );

  return [
    { field: "step", headerName: "Step", maxWidth: 200 },
    {
      field: "status",
      headerName: "Status",
      headerAlign: "center",
      maxWidth: 200
    },
    ...(hasErrorMessages
      ? [
          {
            field: "message",
            headerName: "Message",
            flex: 4
          }
        ]
      : [])
  ];
};

const capitalizeFirstLetter = (val: string) => {
  return String(val).charAt(0) + String(val).substring(1).toLocaleLowerCase();
};

const createJobLogRow = (log: InstanceLogEntry) => {
  const status = log.status || STEPPER_STATES.UNKNOWN;
  return {
    id: `${log.step}-${log.step}-${log.created_at}`,
    step: <EllipsisText>{getStepLabel(log.step || "Unknown")}</EllipsisText>,
    message: <EllipsisText>{log.message}</EllipsisText>,
    isFailed: status === STEPPER_STATES.FAILED,
    status: (
      <Box sx={{ width: "100%", display: "flex", justifyContent: "center" }}>
        <SingleStateStepper
          step={{
            state: status,
            label: capitalizeFirstLetter(status)
          }}
          showToolTip
        />
      </Box>
    )
  };
};

interface InstanceJobLogProps {
  logs: InstanceLogEntry[];
}

export const InstanceJobLog = ({ logs }: InstanceJobLogProps) => (
  <Table
    columns={getColumns(logs)}
    rows={logs.map(createJobLogRow)}
    rowClassName={(params: GridRowClassNameParams<LogDetailsLogsRow>) =>
      params.row?.isFailed ? "error" : null
    }
  />
);
