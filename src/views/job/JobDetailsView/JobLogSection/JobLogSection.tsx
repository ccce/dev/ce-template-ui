import { useState } from "react";
import {
  SimpleAccordion,
  SingleStateStepper,
  STEPPER_STATES
} from "@ess-ics/ce-ui-common";
import { Typography, Stack } from "@mui/material";
import { JobLogTable } from "./JobLogTable";
import { JobDetails, JobStatusResponse } from "../../../../store/templateApi";
import { JOB_STATUS } from "../../../../components/JobDetails/JobDetailsData";

interface JobLogSectionProps {
  jobDetails: JobDetails;
  jobStatus: JobStatusResponse;
}

export const JobLogSection = ({
  jobDetails,
  jobStatus
}: JobLogSectionProps) => {
  const [expanded, setExpanded] = useState(false);
  const status = jobStatus.status || STEPPER_STATES.UNKNOWN;

  const { summary } = jobStatus;

  return (
    <SimpleAccordion
      onChange={() => setExpanded((prev) => !prev)}
      sx={{ marginTop: 0 }}
      summary={
        <Stack
          flexDirection="row"
          justifyContent="center"
          alignItems="center"
          flexWrap="wrap"
          gap={1}
        >
          <Typography
            component="h2"
            variant="h3"
          >
            Job log
          </Typography>
          <SingleStateStepper
            step={{
              state: status,
              label: JOB_STATUS[status].message
            }}
            showToolTip
          />
          {status === STEPPER_STATES.PARTIAL_FAILURE && (
            <Stack flexDirection="row">
              <Typography
                variant="body2"
                sx={{ fontWeight: "600" }}
              >
                {summary?.failed}/
                {(summary?.failed || 0) + (summary?.successful || 0)} failed
              </Typography>
            </Stack>
          )}
        </Stack>
      }
    >
      <>{expanded && <JobLogTable jobDetails={jobDetails} />}</>
    </SimpleAccordion>
  );
};
