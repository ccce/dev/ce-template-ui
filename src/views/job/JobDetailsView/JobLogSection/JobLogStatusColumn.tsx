import { useState, useEffect } from "react";
import {
  Dialog,
  LinearStepper,
  SingleStateStepper,
  STEPPER_STATES
} from "@ess-ics/ce-ui-common";
import { Box, Skeleton } from "@mui/material";
import { InstanceJobLog } from "./InstanceJobLog";
import { DEFAULT_POLLING_INTERVAL_MILLIS } from "../../../../store/apiConfig";
import {
  JobInstance,
  useGetInstanceLogQuery
} from "../../../../store/templateApi";
import { getStepLabel } from "../../../../components/JobDetails/getStepLabel";
import { ApiAlertError } from "../../../../components/common/Alerts/ApiAlertError";

interface JobInstanceStatusColumnProps {
  instance: JobInstance;
  jobId: number;
  isGenerateRevisionJob: boolean;
}

export const JobLogStatusColumn = ({
  instance,
  jobId,
  isGenerateRevisionJob
}: JobInstanceStatusColumnProps) => {
  const [open, setOpen] = useState(false);
  const [stopPolling, setStopPolling] = useState(false);

  const {
    data: instanceLog,
    isLoading,
    isError,
    error
  } = useGetInstanceLogQuery(
    { iocId: instance.id, jobId: jobId },
    { pollingInterval: !stopPolling ? DEFAULT_POLLING_INTERVAL_MILLIS : 0 }
  );
  const convertedTotalStatus = instanceLog?.total_status;
  const isFinished =
    convertedTotalStatus === STEPPER_STATES.FAILED ||
    convertedTotalStatus === STEPPER_STATES.SUCCESSFUL;

  useEffect(() => {
    if (isFinished || isError) {
      setStopPolling(true);
    }
  }, [instanceLog, isFinished, isError]);

  return (
    <>
      {isLoading && <Skeleton width={80} />}
      {error && <ApiAlertError error={error} />}
      {instanceLog && (
        <>
          <Box
            onClick={() => setOpen(true)}
            sx={{ cursor: "pointer", width: "100px" }}
          >
            {isGenerateRevisionJob ? (
              <LinearStepper
                steps={instanceLog?.logs?.map(({ status, step }) => ({
                  state: status,
                  label: getStepLabel(step || "")
                }))}
                activeStep={instanceLog?.logs?.findIndex(
                  ({ step }) => step === instanceLog.current_step
                )}
                isCompleted={isFinished}
              />
            ) : (
              <SingleStateStepper
                step={{
                  state:
                    (instanceLog.logs && instanceLog.logs[0].status) ||
                    "UNKNOWN",
                  label: getStepLabel(
                    (instanceLog.logs && instanceLog.logs[0].step) || "UNKNOWN"
                  )
                }}
                showToolTip
              />
            )}
          </Box>
          <Dialog
            title={`Logs for instance '${instance.instance_name ?? "unknown"}'`}
            content={<InstanceJobLog logs={instanceLog?.logs ?? []} />}
            open={open}
            onClose={() => setOpen(false)}
            DialogProps={{ fullWidth: true, maxWidth: "lg" }}
          />
        </>
      )}
    </>
  );
};
