import { useEffect } from "react";
import { Box } from "@mui/material";
import { Table, EmptyValue, usePagination } from "@ess-ics/ce-ui-common";
import { JobLogStatusColumn } from "./JobLogStatusColumn";
import {
  JobDetails,
  JobInstance,
  useLazyJobInstancesQuery
} from "../../../../store/templateApi";
import { IocGitInfo } from "../../../../components/common/IocGitInfo";
import { ROWS_PER_PAGE } from "../../../../constants";
import { JOB_TYPES } from "../../../../components/JobDetails/JobDetailsData";

interface JobInstancesTableProps {
  jobDetails: JobDetails;
}

const getColumns = (isGenerateRevisionJob: boolean) => {
  return [
    {
      field: "git_repo",
      headerName: "Instances"
    },
    {
      field: "status",
      headerName: "Status",
      align: "center",
      headerAlign: "center",
      flex: isGenerateRevisionJob ? 1 : 0.1
    },
    ...(isGenerateRevisionJob
      ? [
          {
            field: "revision",
            headerName: "Revision",
            align: "right",
            headerAlign: "right"
          }
        ]
      : [])
  ];
};

export const JobLogTable = ({ jobDetails }: JobInstancesTableProps) => {
  const { job_id, job_type, tag_name } = jobDetails;
  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0]
  });
  const isGenerateRevisionJob = job_type === JOB_TYPES.GENERATE_INSTANCE;

  const [callInstanceList, { data, isLoading }] = useLazyJobInstancesQuery();

  useEffect(
    () =>
      setPagination({
        totalCount: data?.total_count ?? 0
      }),
    [setPagination, data?.total_count]
  );

  useEffect(() => {
    callInstanceList({
      jobId: job_id,
      ...pagination
    });
  }, [callInstanceList, pagination, job_id]);

  return (
    <Box sx={{ mt: 1 }}>
      <Table
        columns={getColumns(isGenerateRevisionJob)}
        loading={isLoading}
        rows={
          data?.instances?.map((instance: JobInstance) => {
            return {
              id: instance.id,
              git_repo: instance.instance_name,
              status: (
                <JobLogStatusColumn
                  instance={instance}
                  jobId={job_id}
                  isGenerateRevisionJob={isGenerateRevisionJob}
                />
              ),
              revision:
                instance.git_project_id && tag_name ? (
                  <IocGitInfo
                    gitTag={tag_name}
                    gitProjectId={instance.git_project_id}
                  />
                ) : (
                  <EmptyValue />
                )
            };
          }) || []
        }
        rowHeight={60}
        pagination={pagination}
        onPage={setPagination}
      />
    </Box>
  );
};
