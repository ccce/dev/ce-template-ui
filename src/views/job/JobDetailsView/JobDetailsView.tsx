import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useGlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { IconButton, LinearProgress, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { GenerateRevisionDetails } from "./GenerateRevisionDetails";
import { MoveAttachDetails } from "./MoveAttachDetails";
import { JobStatusSection } from "./JobStatusSection";
import { JobDetailsSection } from "./JobDetailsSection";
import { JobLogSection } from "./JobLogSection/JobLogSection";
import { PATHS } from "../../../components/navigation/Routes";
import { JOB_TYPES } from "../../../components/JobDetails/JobDetailsData";
import { NotFoundView } from "../../../components/navigation/NotFoundView";
import { RootPaper } from "../../../components/common/container/RootPaper";
import { applicationSubTitle } from "../../../components/common/Helper";
import {
  useJobDetailsQuery,
  useJobStatusQuery
} from "../../../store/templateApi";
import { GlobalAppBarContext } from "../../../types/common";
import { DEFAULT_POLLING_INTERVAL_MILLIS } from "../../../store/apiConfig";
import { ApiAlertError } from "../../../components/common/Alerts/ApiAlertError";

export const JobDetailsView = () => {
  const params = useParams();
  const navigate = useNavigate();
  const jobId = parseInt(params.jobId ?? "", 10);
  const { setTitle }: GlobalAppBarContext = useGlobalAppBarContext();
  const [stopPolling, setStopPolling] = useState(false);

  const {
    data: jobDetails,
    error,
    isLoading
  } = useJobDetailsQuery({
    jobId
  });

  const { data: jobStatus, isError } = useJobStatusQuery(
    { jobId: jobDetails?.job_id || 0 },
    {
      pollingInterval: DEFAULT_POLLING_INTERVAL_MILLIS,
      skip: stopPolling || !jobDetails?.job_id
    }
  );

  const notFoundError = error && "status" in error && error?.status === 404;

  useEffect(() => {
    setTitle(
      `CE template ${applicationSubTitle()} / Job Details${
        jobDetails ? `: ${jobDetails?.ioc_type_name}` : ""
      }`
    );
  }, [jobDetails, setTitle]);

  useEffect(() => {
    setStopPolling(!!jobStatus?.finished_at || isError);
  }, [jobStatus, isError]);

  if (notFoundError) {
    return <NotFoundView message={`Job (ID: ${params.jobId}) was not found`} />;
  }

  return (
    <RootPaper>
      {isLoading && <LinearProgress />}
      {error && <ApiAlertError error={error} />}
      {jobDetails && jobStatus && (
        <Stack
          alignItems="start"
          gap={2}
          sx={{ width: "100%" }}
        >
          <IconButton
            color="inherit"
            onClick={() => navigate(PATHS.PATH_JOBS)}
            size="large"
          >
            <ArrowBackIcon />
          </IconButton>
          <Stack
            gap={2}
            sx={{ width: "100%" }}
          >
            <JobStatusSection status={jobStatus.status} />
            {jobDetails.job_type === JOB_TYPES.GENERATE_INSTANCE ? (
              <GenerateRevisionDetails jobDetails={jobDetails} />
            ) : (
              <MoveAttachDetails jobDetails={jobDetails} />
            )}
            <JobDetailsSection
              jobDetails={jobDetails}
              jobStatus={jobStatus}
            />
            <JobLogSection
              jobDetails={jobDetails}
              jobStatus={jobStatus}
            />
          </Stack>
        </Stack>
      )}
    </RootPaper>
  );
};
