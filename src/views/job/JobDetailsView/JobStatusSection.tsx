import { AlertBanner } from "@ess-ics/ce-ui-common";
import { JOB_STATUS } from "../../../components/JobDetails/JobDetailsData";
import { JobStatusResponse } from "../../../store/templateApi";

interface JobStatusSectionProps {
  status: JobStatusResponse["status"];
}

export const JobStatusSection = ({ status }: JobStatusSectionProps) => (
  <AlertBanner
    type={JOB_STATUS[status || "UNKNOWN"].alertType}
    message={`${JOB_STATUS[status || "UNKNOWN"].message}`}
  />
);
