import { KeyValueTable, InternalLink } from "@ess-ics/ce-ui-common";
import { Paper, Box } from "@mui/material";
import { JobIconWithLabel } from "../../../components/job/JobTypes";
import { JobDetails } from "../../../store/templateApi";
import { JOB_TYPES } from "../../../components/JobDetails/JobDetailsData";

interface MoveAttachDetailsProps {
  jobDetails: JobDetails;
}

const getTableRows = (jobDetails: JobDetails) => {
  return {
    ...(jobDetails.job_type === JOB_TYPES.MOVE && {
      "from ioc type": (
        <InternalLink
          to={`/types/${jobDetails.moved_from?.type_id}`}
          label={"IOC Type Details"}
        >
          {jobDetails.moved_from?.type_name}
        </InternalLink>
      )
    }),
    [jobDetails.job_type === JOB_TYPES.MOVE ? "to ioc type" : "ioc type"]: (
      <InternalLink
        to={`/types/${jobDetails.ioc_type_id}`}
        label={"IOC Type Details"}
      >
        {jobDetails.ioc_type_name}
      </InternalLink>
    ),
    "instance count": jobDetails.instance_count
  };
};

export const MoveAttachDetails = ({ jobDetails }: MoveAttachDetailsProps) => (
  <Paper sx={{ padding: "12px 12px 4px 12px", width: "100%" }}>
    <Box sx={{ marginLeft: "6px" }}>
      <JobIconWithLabel jobType={jobDetails.job_type} />
    </Box>
    <KeyValueTable
      obj={getTableRows(jobDetails)}
      variant="overline"
    />
  </Paper>
);
