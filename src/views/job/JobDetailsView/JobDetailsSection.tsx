import { useState } from "react";
import {
  formatDateAndTime,
  SimpleAccordion,
  KeyValueTable,
  Duration,
  EmptyValue
} from "@ess-ics/ce-ui-common";
import { Paper, Typography } from "@mui/material";
import { JobRevisionChip } from "../../../components/job/JobRevisionChip";
import { JobDetails, JobStatusResponse } from "../../../store/templateApi";
import { JOB_TYPES } from "../../../components/JobDetails/JobDetailsData";

interface JobDetailsSectionProps {
  jobDetails: JobDetails;
  jobStatus: JobStatusResponse;
}

const getTimeRows = (jobStatus: JobStatusResponse) => {
  return {
    Started: formatDateAndTime(jobStatus?.created_at),
    Completed: formatDateAndTime(jobStatus?.finished_at),
    Duration: jobStatus ? (
      <Duration
        createOrStartDate={new Date(jobStatus.created_at ?? new Date())}
        finishedAt={new Date(jobStatus.finished_at ?? new Date())}
        textOnly
      />
    ) : (
      <EmptyValue />
    )
  };
};

const getDetailRows = (jobDetails: JobDetails) => {
  const revisions = {
    "Template revision": (
      <JobRevisionChip revision={jobDetails?.template_project} />
    ),
    "Config revision": (
      <JobRevisionChip revision={jobDetails?.configuration_project} />
    )
  };
  return {
    User: jobDetails?.user,
    ...(jobDetails.job_type === JOB_TYPES.GENERATE_INSTANCE && revisions)
  };
};

export const JobDetailsSection = ({
  jobDetails,
  jobStatus
}: JobDetailsSectionProps) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <SimpleAccordion
      onChange={() => setExpanded((prev) => !prev)}
      sx={{ marginTop: 0 }}
      summary={
        <Typography
          component="h2"
          variant="h3"
        >
          Job details
        </Typography>
      }
    >
      <>
        {expanded && (
          <>
            <KeyValueTable
              obj={getDetailRows(jobDetails)}
              variant="overline"
            />
            <Paper sx={{ marginTop: "8px", padding: "12px" }}>
              <KeyValueTable
                obj={getTimeRows(jobStatus)}
                variant="overline"
              />
            </Paper>
          </>
        )}
      </>
    </SimpleAccordion>
  );
};
