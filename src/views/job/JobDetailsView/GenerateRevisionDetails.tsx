import { KeyValueTable, InternalLink } from "@ess-ics/ce-ui-common";
import { Paper, Box } from "@mui/material";
import { JobIconWithLabel } from "../../../components/job/JobTypes";
import { JobDetails } from "../../../store/templateApi";

interface GenerateRevisionDetailsProps {
  jobDetails: JobDetails;
}

const getTableRows = (jobDetails: JobDetails) => {
  return {
    type: (
      <InternalLink
        to={`/types/${jobDetails.ioc_type_id}`}
        label={"IOC Type Details"}
      >
        {jobDetails.ioc_type_name}
      </InternalLink>
    ),
    revision: jobDetails.tag_name,
    "instance count": jobDetails.instance_count
  };
};

export const GenerateRevisionDetails = ({
  jobDetails
}: GenerateRevisionDetailsProps) => {
  return (
    <Paper sx={{ padding: "12px 12px 4px 12px", width: "100%" }}>
      <Box sx={{ marginLeft: "6px" }}>
        <JobIconWithLabel jobType={jobDetails.job_type} />
      </Box>
      <KeyValueTable
        obj={getTableRows(jobDetails)}
        variant="overline"
      />
    </Paper>
  );
};
