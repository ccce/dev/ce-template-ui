import { useSearchParams } from "react-router-dom";
import { CreateTemplateFromBlankSubView } from "./CreateTemplateFromBlankSubView";
import { CreateTemplateFromExampleSubView } from "./CreateTemplateFromExampleSubView";
import CreateTemplateDefaultSubView from "./CreateTemplateDefaultSubView";

export const CreateTemplateView = () => {
  const [params] = useSearchParams();
  const view = params.get("view");

  switch (view) {
    case "blank":
      return <CreateTemplateFromBlankSubView />;
    case "example":
      return <CreateTemplateFromExampleSubView />;
    default:
      return <CreateTemplateDefaultSubView />;
  }
};

export default CreateTemplateView;
