import { useContext, useEffect } from "react";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { applicationSubTitle } from "../../components/common/Helper";
import { CreateFromBlank } from "../../components/templates/CreateTemplate";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";

export const CreateTemplateFromBlankSubView = () => {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);

  useEffect(() => {
    setTitle(
      `CE template ${applicationSubTitle()} / Create IOC Type / From Blank`
    );
  }, [setTitle]);

  return <CreateFromBlank />;
};

export default CreateTemplateFromBlankSubView;
