import { useContext, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { applicationSubTitle } from "../../components/common/Helper";
import { CreateTemplate } from "../../components/templates/CreateTemplate/CreateTemplate";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";

const CreateTemplateDefaultSubView = () => {
  const [, setParams] = useSearchParams();
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);

  // Set the page title, but also reset any url params
  // so that invalid ones don't stay in the url when e.g.
  // navigating backwards in the browser
  useEffect(() => {
    setTitle(`CE template ${applicationSubTitle()} / Create IOC Type`);
    setParams({});
  }, [setTitle, setParams]);

  return <CreateTemplate />;
};

export default CreateTemplateDefaultSubView;
