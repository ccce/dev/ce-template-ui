import { useContext, useEffect } from "react";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { applicationSubTitle } from "../../components/common/Helper";
import { CreateFromExample } from "../../components/templates/CreateTemplate/CreateFromExample";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";

export const CreateTemplateFromExampleSubView = () => {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);

  useEffect(() => {
    setTitle(
      `CE template ${applicationSubTitle()} / Create IOC Type / From Example`
    );
  }, [setTitle]);

  return <CreateFromExample />;
};

export default CreateTemplateFromExampleSubView;
