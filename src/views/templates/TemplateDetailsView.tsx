/**
 * TemplateDetailsView
 * Template details view tab navigation
 */
import { useEffect, useState, ReactNode, SetStateAction } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { LinearProgress, IconButton, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useGlobalAppBarContext, TabPanel } from "@ess-ics/ce-ui-common";
import { PATHS } from "../../components/navigation/Routes";
import { useGetTypeQuery } from "../../store/enhancedApi";
import { useAuthentication } from "../../hooks/useAuthentication";
import { GlobalAppBarContext } from "../../types/common";
import { TemplateStatus } from "../../components/templates/TemplateStatus";
import { TemplateAdmin } from "../../components/templates/TemplateAdmin";
import { RootPaper } from "../../components/common/container/RootPaper";
import { applicationSubTitle } from "../../components/common/Helper";
import { NotFoundView } from "../../components/navigation/NotFoundView";
import { TemplateManagement } from "../../components/templates/TemplateManagement/TemplateManagement";
import { ApiAlertError } from "../../components/common/Alerts/ApiAlertError";

interface Tab {
  label: string;
  content: ReactNode;
}

const getTabs = (typeId: number): Tab[] => [
  {
    label: "Status",
    content: <TemplateStatus typeId={typeId} />
  },
  {
    label: "Management",
    content: <TemplateManagement typeId={typeId} />
  },
  {
    label: "Admin",
    content: <TemplateAdmin typeId={typeId} />
  }
];

export const TemplateDetailsView = () => {
  const { setTitle }: GlobalAppBarContext = useGlobalAppBarContext();
  const { isAdmin, isManager } = useAuthentication();
  const [tabs, setTabs] = useState<Tab[] | null>(null);
  const [tabIndex, setTabIndex] = useState<number>(0);
  const navigate = useNavigate();
  const params = useParams();
  const typeId = Number(params.typeId);

  const { data: template, isLoading, error } = useGetTypeQuery({ typeId });

  useEffect(() => {
    setTitle(
      `CE template ${applicationSubTitle()} / IOC Type Details${
        template ? `: ${template?.name}` : ""
      }`
    );
  }, [template, setTitle]);

  useEffect(() => {
    if (typeId) {
      if (isAdmin) {
        setTabs(getTabs(typeId));
      } else if (isManager) {
        setTabs(getTabs(typeId).filter((tab) => tab.label !== "Admin"));
      } else {
        setTabs(getTabs(typeId).filter((tab) => tab.label === "Status"));
      }
    }
  }, [isAdmin, isManager, typeId]);

  if (!typeId || (error && "status" in error && error.status === 404)) {
    return (
      <NotFoundView message={`IOC type (ID: ${params.typeId}) was not found`} />
    );
  }

  return (
    <RootPaper>
      {isLoading && <LinearProgress />}
      {error && <ApiAlertError error={error} />}
      {tabs && template && (
        <TabPanel
          tabs={tabs}
          initialIndex={tabIndex}
          onTabChange={(index: SetStateAction<number>) => setTabIndex(index)}
          TabsProps={{ centered: true, sx: { flex: 1 } }}
          renderTabs={(tabs: ReactNode) => (
            <Stack
              flexDirection="row"
              justifyContent="space-between"
            >
              <IconButton
                color="inherit"
                onClick={() => navigate(PATHS.PATH_TYPES)}
              >
                <ArrowBackIcon />
              </IconButton>
              {tabs}
            </Stack>
          )}
        />
      )}
    </RootPaper>
  );
};
