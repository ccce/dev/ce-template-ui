import { useEffect, useContext } from "react";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { Grid } from "@mui/material";
import { applicationSubTitle } from "../../components/common/Helper";
import { TemplateTable } from "../../components/templates/TemplateTable";
import { RootPaper } from "../../components/common/container/RootPaper";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";

export function BrowseTemplatesView() {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);

  useEffect(() => {
    setTitle(`CE template ${applicationSubTitle()} / IOC Types`);
  }, [setTitle]);

  return (
    <RootPaper>
      <Grid
        item
        xs={12}
        marginTop={2}
      >
        <TemplateTable />
      </Grid>
    </RootPaper>
  );
}
