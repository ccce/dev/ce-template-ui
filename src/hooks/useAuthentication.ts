import { useEffect, useCallback } from "react";
import { useLazyUserInfoQuery, UserInfo } from "../store/templateApi";
import { useCustomSnackbar } from "../components/common/Snackbar";
import { setUser } from "../store/userSlice";
import { RootState, AppDispatch, AppSelector } from "../store/store";
import { env } from "../config/env";
import { getErrorMessage } from "../components/common/Alerts/ErrorHandling";

const AUTH_IN_PROCESS = "AUTH_IN_PROCESS";
const cacheAuthInProcess = () => localStorage.setItem(AUTH_IN_PROCESS, "ON");
const isAuthInProcess = () => localStorage.getItem(AUTH_IN_PROCESS);
const removeAuthInProcess = () => localStorage.removeItem(AUTH_IN_PROCESS);

export const initLogin = () => {
  cacheAuthInProcess();
  window.open(
    `${env.BE_BASE}${env.API_LOGIN}?redirect_url=${window.location.href}`,
    "_self"
  );
};

export const useAuthentication = () => {
  const { showError } = useCustomSnackbar();
  const dispatch = AppDispatch();
  const user = AppSelector((state: RootState) => state.userInfo) as UserInfo;
  const isLoggedIn = Object.keys(user).length > 0;
  const isAdmin = user.roles?.includes("CE_TemplatingToolAdmin");
  const isManager = user.roles?.every((role) =>
    ["CE_TemplatingToolAdmin", "CE_TemplatingToolIntegrator"].includes(role)
  );

  const [
    getUserInfo,
    { data: userResponse, isUninitialized, isLoading, error }
  ] = useLazyUserInfoQuery({
    pollingInterval: isLoggedIn ? env.TOKEN_RENEW_INTERVAL : undefined
  });

  const login = useCallback(() => {
    try {
      initLogin();
    } catch (error) {
      showError(getErrorMessage(error));
    }
  }, [showError]);

  const logout = useCallback(() => {
    try {
      window.open(
        `${env.BE_BASE}${env.API_LOGOUT}?redirect_url=${window.location.href}`,
        "_self"
      );
    } catch (error) {
      showError(getErrorMessage(error));
    }
  }, [showError]);

  useEffect(() => {
    if (userResponse) {
      dispatch(setUser(userResponse));
      if (isAuthInProcess()) {
        removeAuthInProcess();
      }
    }
  }, [userResponse, dispatch]);

  useEffect(() => {
    if (error && isAuthInProcess()) {
      showError("Login process failed, unable to fetch user information");
      removeAuthInProcess();
    }
  }, [error, showError]);

  return {
    user,
    isUninitialized,
    isLoading,
    isLoggedIn,
    isManager,
    isAdmin,
    getUserInfo,
    login,
    logout
  };
};
