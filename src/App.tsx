import { useEffect } from "react";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import { SnackbarProvider } from "notistack";
import { useGlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { useAuthentication } from "./hooks/useAuthentication";
import { NavigationMenu } from "./components/navigation/NavigationMenu";
import { applicationSubTitle } from "./components/common/Helper";
import { MuiThemeProvider } from "./providers";
import { GlobalAppBarContext } from "./types/common";
import { routes } from "./components/navigation/Routes";

export const App = () => {
  const { setTitle }: GlobalAppBarContext = useGlobalAppBarContext();
  const { getUserInfo, isUninitialized, isLoading } = useAuthentication();

  useEffect(() => {
    getUserInfo();
    setTitle(`CE template ${applicationSubTitle()}`);
  }, [setTitle, getUserInfo]);

  return (
    <SnackbarProvider preventDuplicate>
      <MuiThemeProvider>
        <Router>
          {!isUninitialized && !isLoading && (
            <NavigationMenu>
              <Routes>
                {routes.map((route) => {
                  const { path, Component } = route;
                  return (
                    <Route
                      key={path}
                      path={path}
                      element={<Component />}
                    />
                  );
                })}
              </Routes>
            </NavigationMenu>
          )}
        </Router>
      </MuiThemeProvider>
    </SnackbarProvider>
  );
};
