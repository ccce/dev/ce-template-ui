import { createTheme } from "@mui/material/styles";
import { theme as ceuiTheme } from "@ess-ics/ce-ui-common";

declare module "@mui/material/styles" {
  interface Theme {
    palette: {
      status: {
        ok: {
          main: string;
        };
        progress: {
          main: string;
        };
        fail: {
          main: string;
        };
        disabled: {
          main: string;
        };
      };
      primary: {
        inconsistency: string;
        hoverBackground: string;
        selectionBackground: string;
        selectionFont: string;
        borderColor: string;
      };
      primaryContrastText: { main: string };
    };
  }
}

export const theme = createTheme(ceuiTheme, {
  palette: {
    primary: {
      inconsistency: "#f06360",
      hoverBackground: "#cbdcec",
      selectionBackground: "#9bbcdb",
      selectionFont: "#000080",
      borderColor: "#e4e4e4"
    }
  }
});
