import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { UserInfo } from "./templateApi";

export const userSlice = createSlice({
  name: "userInfo",
  initialState: {},
  reducers: {
    setUser: (
      state,
      action: PayloadAction<UserInfo | Record<string, never>>
    ) => {
      return {
        ...state,
        ...action.payload
      };
    }
  }
});

export const { setUser } = userSlice.actions;
