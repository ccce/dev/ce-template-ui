import { templateApi } from "./templateApi";

const enhancedApiWithTags = templateApi.enhanceEndpoints({
  addTagTypes: ["ArchiveType", "UnArchiveType", "UpdateType", "DeleteTypeIocs"],
  endpoints: {
    listIocs: {
      providesTags: ["DeleteTypeIocs"]
    },
    getType: {
      providesTags: [
        "ArchiveType",
        "UnArchiveType",
        "UpdateType",
        "DeleteTypeIocs"
      ]
    },
    archiveType: {
      invalidatesTags: ["ArchiveType"]
    },
    unarchiveType: {
      invalidatesTags: ["UnArchiveType"]
    },
    updateType: {
      invalidatesTags: ["UpdateType"]
    },
    deleteTypeIocs: {
      invalidatesTags: ["DeleteTypeIocs"]
    }
  }
});

export const {
  useListIocsQuery,
  useGetTypeQuery,
  useUnarchiveTypeMutation,
  useArchiveTypeMutation,
  useUpdateTypeMutation,
  useDeleteTypeIocsMutation
} = enhancedApiWithTags;
