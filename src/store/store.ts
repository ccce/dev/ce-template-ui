import { configureStore } from "@reduxjs/toolkit";
import { useDispatch, useSelector } from "react-redux";
import { templateApi } from "./templateApi";
import { userSlice } from "./userSlice";

export const store = configureStore({
  reducer: {
    // Add the generated reducer as a specific top-level slice
    [templateApi.reducerPath]: templateApi.reducer,
    userInfo: userSlice.reducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(templateApi.middleware)
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatchType = typeof store.dispatch;
export const AppDispatch = useDispatch.withTypes<AppDispatchType>();
export const AppSelector = useSelector.withTypes<RootState>();
