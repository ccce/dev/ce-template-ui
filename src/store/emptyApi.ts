import {
  createApi,
  fetchBaseQuery,
  BaseQueryFn,
  FetchArgs,
  FetchBaseQueryError
} from "@reduxjs/toolkit/query/react";
import { RootState } from "./store";
import { initLogin } from "../hooks/useAuthentication";

const baseQuery = fetchBaseQuery({ baseUrl: "/" });

const BaseQueryWithReauth: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api, extraOptions) => {
  const result = await baseQuery(args, api, extraOptions);
  const state = api.getState() as RootState;
  const userInfo = state.userInfo;

  if (result.error?.status === 401 && Object.keys(userInfo).length > 0) {
    initLogin();
  }

  return result;
};

export const emptySplitApi = createApi({
  baseQuery: BaseQueryWithReauth,
  endpoints: () => ({})
});
