import { emptySplitApi as api } from "./emptyApi";
const injectedRtkApi = api.injectEndpoints({
  endpoints: (build) => ({
    moveInstance: build.mutation<MoveInstanceApiResponse, MoveInstanceApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/iocs/${queryArg.iocInstanceId}/move`,
        method: "PUT"
      })
    }),
    listTypes: build.query<ListTypesApiResponse, ListTypesApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/types`,
        params: {
          createdBy: queryArg.createdBy,
          query: queryArg.query,
          include_archived: queryArg.includeArchived,
          page: queryArg.page,
          limit: queryArg.limit,
          list_all: queryArg.listAll
        }
      })
    }),
    createTypeAndConfiguration: build.mutation<
      CreateTypeAndConfigurationApiResponse,
      CreateTypeAndConfigurationApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types`,
        method: "POST",
        body: queryArg.createTypeRequest,
        params: { example_project_id: queryArg.exampleProjectId }
      })
    }),
    unarchiveType: build.mutation<
      UnarchiveTypeApiResponse,
      UnarchiveTypeApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/unarchive`,
        method: "POST"
      })
    }),
    createIocRevision: build.mutation<
      CreateIocRevisionApiResponse,
      CreateIocRevisionApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/generation`,
        method: "POST",
        body: queryArg.processTypeRequest
      })
    }),
    previewGeneration: build.mutation<
      PreviewGenerationApiResponse,
      PreviewGenerationApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/generation/preview`,
        method: "POST",
        body: queryArg.previewGenerationRequest
      })
    }),
    calculateNewIocs: build.mutation<
      CalculateNewIocsApiResponse,
      CalculateNewIocsApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/generation/calculate_new_iocs`,
        method: "POST",
        body: queryArg.calculateNewIocsRequest
      })
    }),
    attachInstances: build.mutation<
      AttachInstancesApiResponse,
      AttachInstancesApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/attach`,
        method: "POST",
        body: queryArg.attachInstanceRequest
      })
    }),
    getType: build.query<GetTypeApiResponse, GetTypeApiArg>({
      query: (queryArg) => ({ url: `/api/v1/types/${queryArg.typeId}` })
    }),
    deleteType: build.mutation<DeleteTypeApiResponse, DeleteTypeApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}`,
        method: "DELETE"
      })
    }),
    updateType: build.mutation<UpdateTypeApiResponse, UpdateTypeApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}`,
        method: "PATCH",
        body: queryArg.updateTypeRequest
      })
    }),
    getTypeHistory: build.query<
      GetTypeHistoryApiResponse,
      GetTypeHistoryApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/jobs`,
        params: { page: queryArg.page, limit: queryArg.limit }
      })
    }),
    listIocs: build.query<ListIocsApiResponse, ListIocsApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/iocs`,
        params: {
          request_type: queryArg.requestType,
          user: queryArg.user,
          query: queryArg.query,
          page: queryArg.page,
          limit: queryArg.limit,
          list_all: queryArg.listAll
        }
      })
    }),
    deleteTypeIocs: build.mutation<
      DeleteTypeIocsApiResponse,
      DeleteTypeIocsApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/iocs`,
        method: "DELETE",
        body: queryArg.deleteIocRequest,
        params: { force_delete: queryArg.forceDelete }
      })
    }),
    getConfigFiles: build.query<
      GetConfigFilesApiResponse,
      GetConfigFilesApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/generation/validation`,
        params: {
          type_revision: queryArg.typeRevision,
          config_revision: queryArg.configRevision
        }
      })
    }),
    listIocsMinimal: build.query<
      ListIocsMinimalApiResponse,
      ListIocsMinimalApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/generation/minimal`
      })
    }),
    getTypeFileNames: build.query<
      GetTypeFileNamesApiResponse,
      GetTypeFileNamesApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/generation/files`,
        params: { type_revision: queryArg.typeRevision }
      })
    }),
    listExampleProjects: build.query<
      ListExampleProjectsApiResponse,
      ListExampleProjectsApiArg
    >({
      query: () => ({ url: `/api/v1/types/examples` })
    }),
    listTagsAndCommitIds: build.query<
      ListTagsAndCommitIdsApiResponse,
      ListTagsAndCommitIdsApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/repositories/${queryArg.projectId}/tags_and_commits`,
        params: { reference: queryArg.reference }
      })
    }),
    getProjectInformation: build.query<
      GetProjectInformationApiResponse,
      GetProjectInformationApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/repositories/${queryArg.projectId}/info`
      })
    }),
    listProjectFiles: build.query<
      ListProjectFilesApiResponse,
      ListProjectFilesApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/repositories/${queryArg.projectId}/files`,
        params: { reference: queryArg.reference }
      })
    }),
    listTypeProjects: build.query<
      ListTypeProjectsApiResponse,
      ListTypeProjectsApiArg
    >({
      query: () => ({ url: `/api/v1/repositories/type_projects` })
    }),
    fetchFileContent: build.query<
      FetchFileContentApiResponse,
      FetchFileContentApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/repositories/file_content`,
        params: {
          project_id: queryArg.projectId,
          project_reference: queryArg.projectReference,
          file_name: queryArg.fileName
        }
      })
    }),
    listConfigProjects: build.query<
      ListConfigProjectsApiResponse,
      ListConfigProjectsApiArg
    >({
      query: () => ({ url: `/api/v1/repositories/config_projects` })
    }),
    listJobs: build.query<ListJobsApiResponse, ListJobsApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/jobs`,
        params: {
          query: queryArg.query,
          ioc_type_id: queryArg.iocTypeId,
          user: queryArg.user,
          page: queryArg.page,
          limit: queryArg.limit
        }
      })
    }),
    jobStatus: build.query<JobStatusApiResponse, JobStatusApiArg>({
      query: (queryArg) => ({ url: `/api/v1/jobs/${queryArg.jobId}/status` })
    }),
    jobInstances: build.query<JobInstancesApiResponse, JobInstancesApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/jobs/${queryArg.jobId}/iocs`,
        params: { page: queryArg.page, limit: queryArg.limit }
      })
    }),
    getInstanceLog: build.query<
      GetInstanceLogApiResponse,
      GetInstanceLogApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/jobs/${queryArg.jobId}/iocs/${queryArg.iocId}/log`
      })
    }),
    jobDetails: build.query<JobDetailsApiResponse, JobDetailsApiArg>({
      query: (queryArg) => ({ url: `/api/v1/jobs/${queryArg.jobId}/details` })
    }),
    userInfo: build.query<UserInfoApiResponse, UserInfoApiArg>({
      query: () => ({ url: `/api/v1/authentication/user` })
    }),
    oauthLogout: build.query<OauthLogoutApiResponse, OauthLogoutApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/authentication/oauthlogout`,
        params: { redirect_url: queryArg.redirectUrl }
      })
    }),
    oauthLogin: build.query<OauthLoginApiResponse, OauthLoginApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/authentication/oauthlogin`,
        params: { redirect_url: queryArg.redirectUrl }
      })
    }),
    archiveType: build.mutation<ArchiveTypeApiResponse, ArchiveTypeApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/types/${queryArg.typeId}/archive`,
        method: "DELETE"
      })
    })
  }),
  overrideExisting: false
});
export { injectedRtkApi as templateApi };
export type MoveInstanceApiResponse =
  /** status 204 Instance has been moved */ void;
export type MoveInstanceApiArg = {
  /** Move instance from another type to this */
  typeId: number;
  /** IOC Instance ID that should be moved */
  iocInstanceId: number;
};
export type ListTypesApiResponse =
  /** status 200 A paged array of types */ PagedTypeResponse;
export type ListTypesApiArg = {
  /** Search by the field: Created by */
  createdBy?: string;
  /** Search text (Search for Type-name) */
  query?: string;
  /** Show archived entries */
  includeArchived?: boolean;
  /** Page offset - starts with 0 */
  page?: number;
  /** Page size */
  limit?: number;
  listAll?: boolean;
};
export type CreateTypeAndConfigurationApiResponse =
  /** status 201 Type created */ CreateTypeResponse;
export type CreateTypeAndConfigurationApiArg = {
  exampleProjectId?: number;
  createTypeRequest: CreateTypeRequest;
};
export type UnarchiveTypeApiResponse =
  /** status 200 Type is successfully unarchived */ Type;
export type UnarchiveTypeApiArg = {
  /** Id of a Type to unarchive */
  typeId: number;
};
export type CreateIocRevisionApiResponse =
  /** status 202 The creation of IOC instances has started */ OperationStartedResponse;
export type CreateIocRevisionApiArg = {
  /** Type ID */
  typeId: number;
  processTypeRequest: ProcessTypeRequest;
};
export type PreviewGenerationApiResponse =
  /** status 200 IOC preview */ IocPreview;
export type PreviewGenerationApiArg = {
  /** Type ID */
  typeId: number;
  previewGenerationRequest: PreviewGenerationRequest;
};
export type CalculateNewIocsApiResponse =
  /** status 200 Number of new IOCs that will be created */ CalculateNewIocsResponse;
export type CalculateNewIocsApiArg = {
  /** The IOC type ID for which the new IOCs has to be counted */
  typeId: number;
  calculateNewIocsRequest: CalculateNewIocsRequest;
};
export type AttachInstancesApiResponse =
  /** status 200 Instances have been attached */ AttachInstanceResponse[];
export type AttachInstancesApiArg = {
  /** IOC type ID */
  typeId: number;
  attachInstanceRequest: AttachInstanceRequest;
};
export type GetTypeApiResponse = /** status 200 Type descriptor */ Type;
export type GetTypeApiArg = {
  /** Type ID */
  typeId: number;
};
export type DeleteTypeApiResponse =
  /** status 204 Type and config are deleted */ void;
export type DeleteTypeApiArg = {
  /** Id of a Type to delete */
  typeId: number;
};
export type UpdateTypeApiResponse = /** status 200 Type updated */ Type;
export type UpdateTypeApiArg = {
  /** Type ID */
  typeId: number;
  updateTypeRequest: UpdateTypeRequest;
};
export type GetTypeHistoryApiResponse =
  /** status 200 A paged array of type history entries */ PagedTypeHistoryResponse;
export type GetTypeHistoryApiArg = {
  /** Type ID */
  typeId: number;
  /** Page offset - starts with 0 */
  page?: number;
  /** Page size */
  limit?: number;
};
export type ListIocsApiResponse =
  /** status 200 A paged array of IOCs */ PagedIocResponse;
export type ListIocsApiArg = {
  /** Which IOC-list type is requested: ALL, or LATEST (default is LATEST) */
  requestType?: "ALL" | "LATEST";
  /** Type ID */
  typeId: number;
  /** User */
  user?: string;
  /** Query for config-, or creator-name */
  query?: string;
  /** Page offset - starts with 0 */
  page?: number;
  /** Page size */
  limit?: number;
  listAll?: boolean;
};
export type DeleteTypeIocsApiResponse =
  /** status 200 Result of IOC deletion */ NameAndGitRepoWithMessage[];
export type DeleteTypeIocsApiArg = {
  /** Type ID */
  typeId: number;
  /** Force delete from DB regardless if deleting from repo was successful or not */
  forceDelete?: boolean;
  deleteIocRequest: DeleteIocRequest;
};
export type GetConfigFilesApiResponse =
  /** status 200 List of config files (with validation error message in case of validation error) */ ConfigFile[];
export type GetConfigFilesApiArg = {
  /** Type ID */
  typeId: number;
  /** Type revision */
  typeRevision: string;
  /** Configuration revision */
  configRevision: string;
};
export type ListIocsMinimalApiResponse =
  /** status 200 List of generated IOCs */ GeneratedIocNames;
export type ListIocsMinimalApiArg = {
  /** Type ID */
  typeId: number;
};
export type GetTypeFileNamesApiResponse =
  /** status 200 Type file descriptors */ TypeFilesResponse;
export type GetTypeFileNamesApiArg = {
  /** Type ID */
  typeId: number;
  /** Type revision */
  typeRevision: string;
};
export type ListExampleProjectsApiResponse =
  /** status 200 List of Example projects */ GitProject[];
export type ListExampleProjectsApiArg = void;
export type ListTagsAndCommitIdsApiResponse =
  /** status 200 List of Tags and CommitIds for a specific GitLab repo */ GitReference[];
export type ListTagsAndCommitIdsApiArg = {
  /** Git repo project ID */
  projectId: number;
  /** Git reference */
  reference?: string;
};
export type GetProjectInformationApiResponse =
  /** status 200 Git project information */ GitProject;
export type GetProjectInformationApiArg = {
  /** Git repo project ID */
  projectId: number;
};
export type ListProjectFilesApiResponse =
  /** status 200 List of files created in a specific GitLab repo, and reference */ string[];
export type ListProjectFilesApiArg = {
  /** Git repo project ID */
  projectId: number;
  /** Git reference */
  reference?: string;
};
export type ListTypeProjectsApiResponse =
  /** status 200 List of Template projects */ GitProject[];
export type ListTypeProjectsApiArg = void;
export type FetchFileContentApiResponse =
  /** status 200 Content of a template/config file */ FileContentResponse;
export type FetchFileContentApiArg = {
  /** Git project ID */
  projectId: number;
  /** Git reference - optional */
  projectReference?: string;
  /** Filename - optional */
  fileName?: string;
};
export type ListConfigProjectsApiResponse =
  /** status 200 List of Config projects */ GitProject[];
export type ListConfigProjectsApiArg = void;
export type ListJobsApiResponse =
  /** status 200 A paged array of Jobs */ PagedJobResponse;
export type ListJobsApiArg = {
  /** Search text (Search for IOC Type name, Git tag, User) */
  query?: string;
  /** IOC Type ID */
  iocTypeId?: number;
  /** User */
  user?: string;
  /** Page offset - starts with 0 */
  page?: number;
  /** Page size */
  limit?: number;
};
export type JobStatusApiResponse =
  /** status 200 Job status */ JobStatusResponse;
export type JobStatusApiArg = {
  /** The Job ID for which the status should be fetched */
  jobId: number;
};
export type JobInstancesApiResponse =
  /** status 200 Job Instances */ JobInstanceListResponse;
export type JobInstancesApiArg = {
  /** The Job ID for which the instances should be fetched */
  jobId: number;
  /** Page offset - starts with 0 */
  page?: number;
  /** Page size */
  limit?: number;
};
export type GetInstanceLogApiResponse =
  /** status 200 Job instance log */ JobInstanceLogResponse;
export type GetInstanceLogApiArg = {
  /** The Job ID for the IOC Instance that should be fetched */
  jobId: number;
  /** The IOC instance ID for which the log should be fetched */
  iocId: number;
};
export type JobDetailsApiResponse = /** status 200 Job details */ JobDetails;
export type JobDetailsApiArg = {
  /** The Job ID for which the details should be fetched */
  jobId: number;
};
export type UserInfoApiResponse = /** status 200 Ok */ UserInfo;
export type UserInfoApiArg = void;
export type OauthLogoutApiResponse = /** status 200 Ok */ string;
export type OauthLogoutApiArg = {
  /** Redirect url after successful logout */
  redirectUrl?: string;
};
export type OauthLoginApiResponse = /** status 200 Ok */ string;
export type OauthLoginApiArg = {
  /** Redirect url after successful login */
  redirectUrl?: string;
};
export type ArchiveTypeApiResponse =
  /** status 200 Type is successfully archived */ Type;
export type ArchiveTypeApiArg = {
  /** Id of a Type to archive */
  typeId: number;
};
export type GeneralException = {
  error?: string;
  description?: string;
};
export type TypeBase = {
  id: number;
  name: string;
  description?: string;
  deleted: boolean;
  type_project_id: number;
  configuration_project_id?: number;
  created_by: string;
  created_at?: string;
  number_of_iocs?: number;
  latest_ioc_tag?: string;
  latest_operation_id?: number;
};
export type PagedTypeResponse = {
  types?: TypeBase[];
  total_count: number;
  list_size: number;
  page: number;
  page_size?: number;
};
export type CreateTypeResponse = {
  name: string;
  type_project_id: number;
  type_git_id: number;
  config_git_id: number;
};
export type CreateTypeRequest = {
  iocs?: string[];
  /** Description for the IOC Type */
  description?: string;
  /** Only lowercase alphanumeric chars and underscores are allowed in template name (max 20 chars) */
  type_name: string;
};
export type Type = {
  id: number;
  name: string;
  description?: string;
  deleted: boolean;
  type_project_id: number;
  configuration_project_id?: number;
  created_by: string;
  created_at?: string;
  number_of_iocs?: number;
  latest_ioc_tag?: string;
  latest_operation_id?: number;
  type_project_url?: string;
  configuration_project_url?: string;
};
export type OperationStartedResponse = {
  operation_id: number;
};
export type ProcessTypeRequest = {
  type_revision?: string;
  config_revision?: string;
  config_names?: string[];
};
export type IocPreview = {
  project_name?: string;
  configuration_schema?: string;
  substituted_content?: string;
};
export type PreviewGenerationRequest = {
  type_revision?: string;
  template_name?: string;
  config_revision?: string;
  config_name?: string;
};
export type CalculateNewIocsResponse = {
  new_ioc_count: number;
};
export type CalculateNewIocsRequest = {
  config_names?: string[];
};
export type AttachInstanceResponse = {
  result?: string;
  git_project_id?: number;
  new_name?: string;
  instance_id?: number;
};
export type InstancesToAttach = {
  git_project_id?: number;
  new_name?: string;
};
export type AttachInstanceRequest = {
  attach_instances?: InstancesToAttach[];
};
export type UpdateTypeRequest = {
  description?: string;
  type_new_name?: string;
};
export type TypeHistoryEntry = {
  event?: "CREATE" | "UPDATE" | "DELETE" | "RESTORE";
  type_id: number;
  type_name: string;
  created_by?: string;
  type_project_id: number;
  configuration_project_id: number;
  updated_by?: string;
  updated_at?: string;
};
export type PagedTypeHistoryResponse = {
  historyEntries?: TypeHistoryEntry[];
  total_count: number;
  list_size: number;
  page: number;
  page_size?: number;
};
export type Ioc = {
  id: number;
  type_id: number;
  type_name: string;
  created_by: string;
  created_at?: string;
  git_project_id?: number;
  git_tag?: string;
  configuration_name?: string;
  version?: number;
};
export type PagedIocResponse = {
  total_count: number;
  list_size: number;
  page: number;
  page_size?: number;
  iocs?: Ioc[];
  type_id: number;
};
export type NameAndGitRepoWithMessage = {
  message?: string;
  result: "SUCCESS" | "ERROR";
  configuration_name: string;
  git_project_id: number;
};
export type NameAndGitRepo = {
  configuration_name: string;
  git_project_id: number;
};
export type DeleteIocRequest = {
  iocs_to_delete?: NameAndGitRepo[];
};
export type ConfigFile = {
  file_name?: string;
  is_valid?: boolean;
  error_message?: string;
};
export type GeneratedIocNames = {
  iocs?: NameAndGitRepo[];
  list_size: number;
  type_id: number;
};
export type TypeFilesResponse = {
  type_id: number;
  type_revision: string;
  type_files?: string[];
  files_to_copy?: string[];
  ignored_files?: string[];
};
export type GitProject = {
  project_id: number;
  project_name: string;
  project_url: string;
  description?: string;
};
export type GitReference = {
  reference: string;
  description?: string;
  type: "TAG" | "COMMIT" | "UNKNOWN";
  short_reference?: string;
  commit_date: string;
};
export type FileContentResponse = {
  content?: string;
};
export type Job = {
  id: number;
  status?: "QUEUED" | "RUNNING" | "FAILED" | "PARTIAL_FAILURE" | "SUCCESSFUL";
  ioc_type_name: string;
  created_by?: string;
  tag_name?: string;
  created_at?: string;
  finished_at?: string;
  instance_count: number;
  ioc_type_id: number;
  job_type: "GENERATE_INSTANCE" | "ATTACH" | "MOVE";
};
export type PagedJobResponse = {
  jobs?: Job[];
  total_count: number;
  list_size: number;
  page: number;
  page_size?: number;
};
export type SummaryResponse = {
  successful?: number;
  failed?: number;
};
export type JobStatusResponse = {
  status?: "QUEUED" | "RUNNING" | "FAILED" | "PARTIAL_FAILURE" | "SUCCESSFUL";
  summary?: SummaryResponse;
  job_id: number;
  created_at?: string;
  finished_at?: string;
};
export type JobInstance = {
  id: number;
  instance_name: string;
  git_project_id: number;
};
export type JobInstanceListResponse = {
  total_count: number;
  list_size: number;
  page: number;
  page_size?: number;
  instances?: JobInstance[];
  job_id: number;
};
export type InstanceLogEntry = {
  step?:
    | "CREATE_GIT_PROJECT"
    | "PROCESSING_TEMPLATE_AND_COMMIT"
    | "ATTACH_INSTANCE"
    | "MOVE_INSTANCE";
  message?: string;
  step_index?: number;
  created_at?: string;
  finished_at?: string;
  status?: "QUEUED" | "SKIPPED" | "FAILED" | "RUNNING" | "SUCCESSFUL";
};
export type JobInstanceLogResponse = {
  logs?: InstanceLogEntry[];
  instance_id: number;
  current_step?:
    | "CREATE_GIT_PROJECT"
    | "PROCESSING_TEMPLATE_AND_COMMIT"
    | "ATTACH_INSTANCE"
    | "MOVE_INSTANCE";
  total_status?: "QUEUED" | "SKIPPED" | "FAILED" | "RUNNING" | "SUCCESSFUL";
};
export type InstanceParent = {
  type_id?: number;
  type_name?: string;
};
export type GitProjectInfo = {
  reference?: string;
  reference_url?: string;
  reference_type?: "TAG" | "COMMIT" | "UNKNOWN";
};
export type JobDetails = {
  user?: string;
  job_id: number;
  job_type: "GENERATE_INSTANCE" | "ATTACH" | "MOVE";
  ioc_type_name: string;
  ioc_type_id: number;
  moved_from?: InstanceParent;
  tag_name?: string;
  template_project?: GitProjectInfo;
  configuration_project?: GitProjectInfo;
  instance_count?: number;
  created_at?: string;
  finished_at?: string;
};
export type UserInfo = {
  fullName?: string;
  loginName: string;
  avatar?: string;
  email?: string;
  gitlabUserName?: string;
  roles: string[];
};
export const {
  useMoveInstanceMutation,
  useListTypesQuery,
  useLazyListTypesQuery,
  useCreateTypeAndConfigurationMutation,
  useUnarchiveTypeMutation,
  useCreateIocRevisionMutation,
  usePreviewGenerationMutation,
  useCalculateNewIocsMutation,
  useAttachInstancesMutation,
  useGetTypeQuery,
  useLazyGetTypeQuery,
  useDeleteTypeMutation,
  useUpdateTypeMutation,
  useGetTypeHistoryQuery,
  useLazyGetTypeHistoryQuery,
  useListIocsQuery,
  useLazyListIocsQuery,
  useDeleteTypeIocsMutation,
  useGetConfigFilesQuery,
  useLazyGetConfigFilesQuery,
  useListIocsMinimalQuery,
  useLazyListIocsMinimalQuery,
  useGetTypeFileNamesQuery,
  useLazyGetTypeFileNamesQuery,
  useListExampleProjectsQuery,
  useLazyListExampleProjectsQuery,
  useListTagsAndCommitIdsQuery,
  useLazyListTagsAndCommitIdsQuery,
  useGetProjectInformationQuery,
  useLazyGetProjectInformationQuery,
  useListProjectFilesQuery,
  useLazyListProjectFilesQuery,
  useListTypeProjectsQuery,
  useLazyListTypeProjectsQuery,
  useFetchFileContentQuery,
  useLazyFetchFileContentQuery,
  useListConfigProjectsQuery,
  useLazyListConfigProjectsQuery,
  useListJobsQuery,
  useLazyListJobsQuery,
  useJobStatusQuery,
  useLazyJobStatusQuery,
  useJobInstancesQuery,
  useLazyJobInstancesQuery,
  useGetInstanceLogQuery,
  useLazyGetInstanceLogQuery,
  useJobDetailsQuery,
  useLazyJobDetailsQuery,
  useUserInfoQuery,
  useLazyUserInfoQuery,
  useOauthLogoutQuery,
  useLazyOauthLogoutQuery,
  useOauthLoginQuery,
  useLazyOauthLoginQuery,
  useArchiveTypeMutation
} = injectedRtkApi;
