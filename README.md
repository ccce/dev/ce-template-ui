# CE template web UI

This project provides a web interface for the [CE template back-end](https://gitlab.esss.lu.se/ccce/dev/ce-template-backend), written in React.

## Quickstart

```shell
npm ci
npm start
```

## Requirements

- NodeJS and npm
- Credentials from ESS' Artifactory, with permissions to the ics-npm registry

## Configuration

All necessary configuration is stored in a config file in the `public` folder, called [config.js](public/config.js). The config file has to be included in the [index.html](index.html) file.

The following values can be set in the file:

| Environment variable | Description                                                                                                                                                                                  |
| ------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `SERVER_ADDRESS`            | The backend server base URL _(host:port)_ (if backend server is running natively on the same host as the UI then the value has to be empty, and proxy has to be set!)                        |
| `API_BASE_ENDPOINT`         | The context path of the application's REST API _(e.g. /api/spec)_                                                                                                                            |
| `TOKEN_RENEW_INTERVAL`      | Time interval to renew JWT auth token _(in milliseconds)_                                                                                                                                    |
| `ENVIRONMENT_TITLE`         | Adds a (sub)text to the HTLM title, and also for the appBar header. Supposed to show information to the user which environment they are using. The default value is empty. _Optional field!_ |
| `TEMPLATE_FRONTEND_VERSION` | Stores the frontend's version (deployed tag-, or branchname). (Value will be overwritten with actual value after the application is deployed)                                                |
| `SUPPORT_URL`               | The support URL for users requesting help / reporting issues                                                                                                                                 |
| `API_LOGIN`                 | The oauth url for login                                                                                                                                                                      |
| `API_LOGOUT`                | The oauth url for logout                                                                                                                                                                     |
| `BE_BASE`                   | The base url for the oauth process                                                                                                                                                           |

Refering to a value in the JS source code is: _window.field-name_ (e.g. `window.SERVER_ADDRESS`).

## Development

See [DEVELOPMENT.md](DEVELOPMENT.md).
