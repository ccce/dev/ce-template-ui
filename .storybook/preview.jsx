/** @type { import('@storybook/react').Preview } */
import { MuiThemeProvider } from "../src/providers";
import { StateProvider } from "../src/store";
import { withActions } from "@storybook/addon-actions/decorator";

const preview = {
  parameters: {
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i
      }
    }
  },
  decorators: [
    withActions,
    (Story) => (
      <StateProvider>
        <MuiThemeProvider>
          <Story />
        </MuiThemeProvider>
      </StateProvider>
    )
  ]
};

export default preview;
