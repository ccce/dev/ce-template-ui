/* eslint-disable */

// These values are overridden / replaced by a file
// generated during runtime. The below are example values
// and do not reflect current/real values.
SERVER_ADDRESS = "";
ENVIRONMENT_TITLE = "LOCAL";
API_BASE_ENDPOINT = "/api/spec";
SWAGGER_UI_URL = "/api/docs";
TOKEN_RENEW_INTERVAL = 180000;
SUPPORT_URL =
  "https://jira.esss.lu.se/plugins/servlet/desk/portal/44?requestGroup=137";
TEMPLATE_FRONTEND_VERSION = "1.2.3-local";
BE_BASE = "http://localhost:8080";
API_LOGIN = "/api/v1/authentication/oauthlogin";
API_LOGOUT = "/api/v1/authentication/oauthlogout";
