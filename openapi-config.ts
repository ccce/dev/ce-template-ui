import type { ConfigFile } from "@rtk-query/codegen-openapi";

const config: ConfigFile = {
  schemaFile: "http://localhost:8080/api/spec",
  apiFile: "./src/store/emptyApi.ts",
  apiImport: "emptySplitApi",
  outputFile: "./src/store/templateApi.ts",
  exportName: "templateApi",
  hooks: { queries: true, lazyQueries: true, mutations: true }
};

export default config;
